SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS Addresses;

CREATE TABLE Addresses(
AddressID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
Address VARCHAR(320)
);
SET FOREIGN_KEY_CHECKS=1;

INSERT INTO Addresses (Address) VALUES
('mary@read.com'),
('bonnie@fox.com'),
('atalante@hunt.com'),
('skadi@giant.com'),
('jimbo@com.com');
