

CREATE DATABASE IF NOT EXISTS EmailClient;

USE EmailClient;

DROP USER IF EXISTS mailer@localhost;
CREATE USER mailer@localhost IDENTIFIED BY 'ayemail';
GRANT ALL ON EmailClient.* TO mailer@localhost;


FLUSH PRIVILEGES;
