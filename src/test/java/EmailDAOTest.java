
import com.adrianyotov.properties.EmailBean;
import com.adrianyotov.properties.MailConfigBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.*;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import persistence.*;



/**
 * Test class for EmailDAOImpl's database CRUD methods
 * 
 * @author Adrian Yotov
 */
//@Ignore
public class EmailDAOTest {
    
    public ExpectedException thrown = ExpectedException.none();
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOTest.class);
    private MailConfigBean bean;
    private String URL;
    
    @Before
    public void setUp() {
        bean = new MailConfigBean("", "", "", "", "", "", "", "localhost", "EmailClient", "3306", "root", "dawson");
        this.URL = "jdbc:mysql://"+bean.getUrlDB()+":"+ bean.getMysqlPort()+"/"+ bean.getMysqlDatabase();;
        seedDatabase("createEmailClient.sql");
        seedDatabase("createAddressesTable.sql");
        seedDatabase("createFolderTable.sql");
        seedDatabase("createMailTables.sql");
        seedDatabase("createAttachmentsTable.sql");
        seedDatabase("EmailToAddressBridging.sql");     
        bean.setDbUserName("mailer");
        bean.setDbPassword("ayemail");
    }
    
    private void seedDatabase(String filename){
        LOG.info("@Before seeding");
        final String seedDataScript = loadAsString(filename);
        
        try(Connection connection = DriverManager.getConnection(URL, bean.getDbUserName(), bean.getDbPassword())){
            for (String stmt : splitStatements(new StringReader(seedDataScript), ";")){
                connection.prepareStatement(stmt).execute();
            }
        } catch (SQLException e){
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    private String loadAsString(final String path) {
        try (InputStream inpSt = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner sc = new Scanner(inpSt) ){
            return sc.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("unable to close input stream.", e);
        }
    }
    
    private List<String> splitStatements(Reader reader, String stmtDelimiter){
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(stmtDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }
    
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
    
    
    @Test
    public void testFindAll() throws SQLException {
        LOG.info("findAll Test Running");
        EmailDAO mail = new EmailDAOImpl(bean);
        ArrayList<EmailBean> findAllTest = mail.findAll();
        LOG.info("Finished findall");
        assertEquals(5, findAllTest.size());
    }
    
    @Test
    public void findSearchTerm() throws SQLException {
        LOG.info("Testing search term");
        String term = "sbj";
        EmailDAO mail = new EmailDAOImpl(bean);
        List<EmailBean> mailTest = mail.findSearchTerm(term);
        assertEquals(mailTest.get(0).email.subject(), term);
    }
    
    @Test 
    public void testFindID() throws SQLException, ParseException {
        EmailDAO mail = new EmailDAOImpl(bean);
        
        EmailBean foundMail = mail.findID(1);
        EmailBean expectMail = new EmailBean();
        
        assertEquals(foundMail.email.from().toString(), "jimbo@com.com");
    }
    
    @Test
    public void createEmailTest() throws SQLException {
        EmailBean newMail =  new EmailBean();
        newMail.email.from("jimbo@com.com");
        newMail.email.to("bonnie@fox.com");
        newMail.email.cc("atalante@hunt.com");
        newMail.email.bcc("someone@somewhere.com");
        newMail.email.textMessage("wew");
        newMail.email.htmlMessage("<p>wow</p>");
        newMail.email.subject("subbed");
        newMail.setID(1);
        newMail.setFolderID(1);
        newMail.setRecDate(LocalDateTime.now());
        newMail.setSentDate(null);
        EmailDAO mail = new EmailDAOImpl(bean);
        mail.createEmail(newMail);
        EmailBean retrieve = mail.findID(newMail.getID());
        assertTrue(newMail.email.subject().equals( retrieve.email.subject())
                   && newMail.getID() == 6);
    }
    
    @Test(expected = SQLException.class)
    public void deleteMainFolderTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        mail.deleteFolder("Draft");
    }
    
    @Test
    public void deleteNonMainFolderTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        int result = mail.deleteFolder("Unnecessary");
        assertEquals(1, result);
    }
    
    @Test
    public void updateDraftToSentTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        int result = mail.updateDraftToSend(4);
        assertEquals(1, result);
    }
    
    @Test(expected = SQLException.class)
    public void createMainFolderTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        int result = mail.createFolder("Inbox");
        
    }
    
    @Test 
    public  void createNonMainFolderTest() throws SQLException {
                EmailDAO mail = new EmailDAOImpl(bean);
                int result = mail.createFolder("Testing");
                assertEquals(1, result);
    }
    
    @Test(expected = SQLException.class)
    public void createExistingFolderTest() throws SQLException {
                EmailDAO mail = new EmailDAOImpl(bean);
                int result = mail.createFolder("Unnecessary");
    }
    
    @Test
    public void sameCreatedEmailTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        EmailBean mailData = new EmailBean();
        mailData.email.from("someone@some.com");
        mailData.email.to("newguy@newman.com");
        mailData.email.cc("sam@sam.com");
        mailData.email.bcc("jon@jon.com");
        mailData.email.textMessage("wow");
        mailData.email.htmlMessage("<p> look at this</p>");
        mailData.email.subject("impressive");
        mailData.setID(1);
        mailData.setFolderID(1);
        mailData.setRecDate(LocalDateTime.now());
        mailData.setSentDate(null);
        mailData.email.subject("same?");
        int result = mail.createEmail(mailData);
        EmailBean received = mail.findID(6);
        assertTrue(mailData.email.from().toString().equals(received.email.from().toString())
                    && mailData.email.subject().equals(received.email.subject())
                    && received.getID() == 6);
    }
    
    @Test 
    public void deleteEmailTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        int result =  mail.deleteEmail(5);
        assertEquals(1, result);
    }
    
    @Test 
    public void deleteFolderTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        int result = mail.deleteFolder("Unnecessary");
        assertEquals(result, 1);
    }
    
    @Test
    public void updateDraftTest() throws SQLException {
        EmailDAO mail = new EmailDAOImpl(bean);
        EmailBean email = new EmailBean();
        email.setID(4);
        email.email.subject("newsubj");
        email.email.textMessage("updated");
        email.email.htmlMessage("<p>new and improved</p>");
        int result = mail.updateDraft(email);
        assertEquals(result, 1);
    }
    
    
}
