
import com.adrianyotov.business.SendAndReceive;
import com.adrianyotov.properties.MailConfigBean;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.AuthenticationFailedException;
import jodd.mail.Email;
import jodd.mail.ReceivedEmail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Adrian Yotov
 */
public class SendReceiveTest {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SendReceiveTest.class);

    MailConfigBean sendUser = new MailConfigBean();
    ;
    MailConfigBean receiveUser = new MailConfigBean();
    ;
    MailConfigBean ccUser = new MailConfigBean();
    ;
    MailConfigBean bccUser = new MailConfigBean();
    ;
    ArrayList<String> toList = new ArrayList<>();
    ArrayList<String> ccList = new ArrayList<>();
    ArrayList<String> bccList = new ArrayList<>();
    ArrayList<String> embAtt = new ArrayList<>();
    ArrayList<File> regAtt = new ArrayList<>();

    /**
     * Setting up regular users for quick using
     */
    @Before
    public void SetUp() {
        //sendUser = new MailConfigBean("smtp.gmail.com", "testoneadrian1@gmail.com", "1734063dawson");
        //receiveUser = new MailConfigBean("imap.gmail.com", "testtwoadrian2@gmail.com", "1734063dawson2");
        //ccUser = new MailConfigBean("imap.gmail.com", "testthreeadrian3@gmail.com", "1734063dawson3");
        //bccUser = new MailConfigBean("imap.gmail.com", "testfouradrian4@gmail.com", "1734063dawson4");
        sendUser.setUserEmailAddress("testoneadrian1@gmail.com");
        sendUser.setUrlSMTP("smtp.gmail.com");
        sendUser.setMailPassword("1734063dawson");

        receiveUser.setUserEmailAddress("testtwoadrian2@gmail.com");
        receiveUser.setUrlIMAP("imap.gmail.com");
        receiveUser.setMailPassword("1734063dawson2");

        ccUser.setUserEmailAddress("testthreeadrian3@gmail.com");
        ccUser.setUrlIMAP("imap.gmail.com");
        ccUser.setMailPassword("1734063dawson3");

        bccUser.setUserEmailAddress("testfouradrian4@gmail.com");
        bccUser.setUrlIMAP("imap.gmail.com");
        bccUser.setMailPassword("1734063dawson4");

    }

    // Test to send an email with only one recipient
    @Test
    public void testSendEmailOneTo() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>boop";
        String subj = "Test1";
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, new ArrayList<>(), new ArrayList<>(), subj, msg, htmlMsg);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(receiveUser);
        assertTrue(sendUser.getUserEmailAddress().equals(emails[emails.length - 1].from().toString())
                && receiveUser.getUserEmailAddress().equals(emails[emails.length - 1].to()[0].toString())
                && emails[emails.length - 1].subject().equals(subj));
    }

    // Test to send one email to many "To" recipients
    @Test
    public void testSendEmailManyTo() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>bump";
        String subj = "Many To Test";
        toList.add(receiveUser.getUserEmailAddress());
        toList.add(ccUser.getUserEmailAddress());
        toList.add(bccUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(bccUser);
        assertTrue(sendUser.getUserEmailAddress().equals(emails[emails.length - 1].from().toString())
                && bccUser.getUserEmailAddress().equals(emails[emails.length - 1].to()[2].toString())
                && emails[emails.length - 1].subject().equals(subj));
    }

    // Test to send one email to "To", "CC" and "BCC" users
    @Test
    public void testSendEmailToCcBcc() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>ding";
        String subj = "ToCcBcc Test";

        toList.add(receiveUser.getUserEmailAddress());
        ccList.add(ccUser.getUserEmailAddress());
        bccList.add(bccUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(bccUser);
        assertEquals(emails[emails.length-1].subject(), subj);
    }

    // Test to see if the SendAndReceive method "EmailCheck" works as intended
    @Test(expected = Exception.class)
    public void testEmailCheckMethod() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>jump";
        String subj = "Email Check Test";

        toList.add(receiveUser.getUserEmailAddress());
        toList.add(ccUser.getUserEmailAddress());
        toList.add("blarg");
        ccList.add("aaa");
        ccList.add(bccUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);

    }

    // Test to see if null mail properties are accepted, testing SendAndReceive nullToEmpty method at the same time
    @Test
    public void testNullProperties() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = null;
        String htmlMsg = null;
        String subj = null;
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        assertTrue(email.subject().isEmpty());
    }

    // Test to see the email will throw an exception for the wrong credentials
    @Test(expected = Exception.class)
    public void testBadSender() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = null;
        String htmlMsg = null;
        String subj = null;
        toList.add(receiveUser.getUserEmailAddress());
        MailConfigBean badUser = new MailConfigBean();
        serTest.sendEmail(badUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
    }

    // Test to see if the email will be null since there are no receivers
    @Test
    public void testNoReceivers() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "bump";
        String htmlMsg = "<h3>yump";
        String subj = "No receivers";
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        assertEquals(email.subject(), "No receivers");
    }

    // Test to see if the send works but the receiver's credentials are incorrect
    @Test(expected = Exception.class)
    public void testGoodSendBadReceive() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = null;
        String htmlMsg = null;
        String subj = null;
        toList.add("fjkhfrujojldfghudfjog.com");
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
    }

    // Test to see if an embedded attachment is sent properly
    @Test
    public void testEmbeddedAttachmentReceived() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>more tests "
                + "<img src='cid:FreeFall.jpg'>";
        String subj = "Attach Test";
        embAtt.add("FreeFall.jpg");
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(receiveUser);
        assertEquals(embAtt.get(0), emails[emails.length - 1].attachments().get(0).getEncodedName());
    }

    // Test to see if a regular attachment is sent properly
    @Test
    public void testRegularAttachmentReceived() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>wow! ";
        String subj = "Another Attach Test";
        regAtt.add(new File("FreeFall.jpg"));
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(receiveUser);
        assertEquals(regAtt.get(0).getName(), emails[emails.length - 1].attachments().get(0).getEncodedName());
    }

    // Test to see if multiple regular attachments are sent properly
    @Test
    public void testMultipleRegularAttachmentsReceived() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>wow! ";
        String subj = "Plus Attach Test";
        regAtt.add(new File("FreeFall.jpg"));
        regAtt.add(new File("WindsorKen180.jpg"));
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(receiveUser);
        assertEquals(regAtt.get(1).getName(), emails[emails.length - 1].attachments().get(1).getEncodedName());
    }

    // Test to see if multiple embedded attachments are sent properly
    @Test
    public void testMultipleEmbeddedAttachmentsReceived() throws Exception {
        SendAndReceive serTest = new SendAndReceive();
        String msg = "plain text here";
        String htmlMsg = "<h3>tey naiga "
                + "<img src='cid:FreeFall.jpg'>"
                + "<img src='cid:WindsorKen180.jpg'>";
        String subj = "Emb Attach Tests";
        embAtt.add("FreeFall.jpg");
        embAtt.add("WindsorKen180.jpg");
        toList.add(receiveUser.getUserEmailAddress());
        Email email = serTest.sendEmail(sendUser, toList, ccList, bccList, regAtt, embAtt, subj, msg, htmlMsg);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOG.debug("Thread sleep failed");
        }
        ReceivedEmail[] emails = serTest.receiveEmail(receiveUser);
        assertEquals(embAtt.get(1).toString(), emails[emails.length - 1].attachments().get(1).getEncodedName());
    }
}
