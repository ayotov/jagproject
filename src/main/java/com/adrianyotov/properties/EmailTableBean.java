/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Adrian Yotov
 */
public class EmailTableBean {

    private IntegerProperty id;
    private IntegerProperty fID;
    private StringProperty from;
    private StringProperty msg;
    private StringProperty htmlMsg;
    private StringProperty subj;
    private ObjectProperty recDate;
    private ObjectProperty sendDate;
    private ArrayList<StringProperty> to;
    private ArrayList<StringProperty> cc;
    private ArrayList<StringProperty> bcc;

    public EmailTableBean(int id, int fID, String from, String msg, String htmlMsg, String subj, LocalDateTime recDate, LocalDateTime sendDate, List<String> to, List<String> cc, List<String> bcc) {
        this.id = new SimpleIntegerProperty(id);
        this.fID = new SimpleIntegerProperty(fID);
        this.from = new SimpleStringProperty(from);
        this.msg = new SimpleStringProperty(msg);
        this.htmlMsg = new SimpleStringProperty(htmlMsg);
        this.subj = new SimpleStringProperty(subj);
        this.recDate = new SimpleObjectProperty(recDate);
        this.sendDate = new SimpleObjectProperty(sendDate);
        this.to = new ArrayList<>();
        this.cc = new ArrayList<>();
        this.bcc = new ArrayList<>();

        if (to != null) {
            for (String sentTo : to) {
                this.to.add(new SimpleStringProperty(sentTo));
            }
        }

        if (cc != null) {
            for (String sentCC : cc) {
                this.cc.add(new SimpleStringProperty(sentCC));
            }
        }

        if (bcc != null) {

            for (String sentBCC : bcc) {
                this.bcc.add(new SimpleStringProperty(sentBCC));
            }
        }
    }

    public EmailTableBean() {
        this.id = new SimpleIntegerProperty(-1);
        this.fID = new SimpleIntegerProperty(-1);
        this.from = new SimpleStringProperty("");
        this.msg = new SimpleStringProperty("");
        this.htmlMsg = new SimpleStringProperty("");
        this.subj = new SimpleStringProperty("");
        this.recDate = new SimpleObjectProperty(null);
        this.sendDate = new SimpleObjectProperty(null);
        this.to = new ArrayList<>();
        this.cc = new ArrayList<>();
        this.bcc = new ArrayList<>();
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getfID() {
        return fID.get();
    }

    public void setfID(int fID) {
        this.fID.set(fID);
    }

    public String getFrom() {
        return from.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public StringProperty fromProperty() {
        return from;
    }

    public String getMsg() {
        return this.msg.get();
    }

    public StringProperty msgProperty() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg.set(msg);
    }

    public StringProperty htmlMsgProperty() {
        return htmlMsg;
    }

    public void setHtmlMsg(StringProperty htmlMsg) {
        this.htmlMsg = htmlMsg;
    }

    public String getHtmlMsg() {
        return this.htmlMsg.get();
    }

    public String getSubj() {
        return subj.get();
    }

    public void setSubj(String subj) {
        this.subj.set(subj);
    }

    public StringProperty subjProperty() {
        return subj;
    }

    public LocalDateTime getRecDate() {
        return (LocalDateTime) recDate.get();
    }

    public void setRecDate(LocalDateTime date) {
        this.recDate.set(date);
    }

    public ObjectProperty recDateProperty() {
        return this.recDate;
    }

    public LocalDateTime getSendDate() {
        return (LocalDateTime) sendDate.get();
    }

    public void setSendDate(LocalDateTime date) {
        this.sendDate.set(date);
    }

    public ObjectProperty sendDateProperty() {
        return this.sendDate;
    }

    public ArrayList<StringProperty> getToProperty() {
        return this.to;
    }

    public ArrayList<String> getTo() {
        ArrayList<String> toReturn = new ArrayList<>();
        for (StringProperty rec : this.to) {
            toReturn.add(rec.get());
        }
        return toReturn;
    }

    public void setTo(ArrayList<StringProperty> to) {
        this.to = to;
    }

    public ArrayList<StringProperty> getCCProperty() {
        return this.cc;
    }

    public ArrayList<String> getCC() {
        ArrayList<String> ccReturn = new ArrayList<>();
        for (StringProperty rec : this.cc) {
            ccReturn.add(rec.get());
        }
        return ccReturn;
    }

    public void setCC(ArrayList<StringProperty> cc) {
        this.cc = cc;
    }

    public List<StringProperty> getBCCProperty() {
        return this.bcc;
    }

    public ArrayList<String> getBCC() {
        ArrayList<String> bccReturn = new ArrayList<>();
        for (StringProperty rec : this.bcc) {
            bccReturn.add(rec.get());
        }
        return bccReturn;
    }

    public void setBCC(ArrayList<StringProperty> bcc) {
        this.bcc = bcc;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailTableBean other = (EmailTableBean) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.subj, other.subj)) {
            return false;
        }
        if (!Objects.equals(this.recDate, other.recDate)) {
            return false;
        }

        return true;
    }

}
