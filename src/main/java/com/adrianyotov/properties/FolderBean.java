/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties;

import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author grimm
 */
public class FolderBean {
    private IntegerProperty id;
    private StringProperty fName;

    public FolderBean(int id, String fName) {
        this.id = new SimpleIntegerProperty(id);
        this.fName = new  SimpleStringProperty(fName);
    }
    public FolderBean(){
        this(-1, "");
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getfName() {
        return fName.get();
    }

    public void setfName(String fName) {
        this.fName.set(fName);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FolderBean other = (FolderBean) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.fName, other.fName)) {
            return false;
        }
        return true;
    }
    
    
    
}
