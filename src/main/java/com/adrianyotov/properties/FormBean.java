/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties;
import java.io.File;
import java.util.Objects;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Adrian Yotov
 */
public class FormBean {
    private StringProperty to;
    private StringProperty cc;
    private StringProperty bcc;
    private StringProperty subj;
    private ListProperty attachments;

    public FormBean(String to, String cc, String bcc, String subj, ObservableList<File> atts) {
        this.to = new SimpleStringProperty(to);
        this.cc = new SimpleStringProperty(cc);
        this.bcc = new SimpleStringProperty(bcc);
        this.subj = new SimpleStringProperty(subj);
        this.attachments = new SimpleListProperty(atts);
    }

    public String getTo() {
        return to.get();
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public String getCc() {
        return cc.get();
    }

    public void setCc(String cc) {
        this.cc.set(cc);
    }

    public String getBcc() {
        return bcc.get();
    }

    public void setBcc(String bcc) {
        this.bcc.set(bcc);
    }

    public String getSubj() {
        return subj.get();
    }

    public void setSubj(String subj) {
        this.subj.set(subj);
    }
    
    public ObservableList<File> gatAttachments(){
        return this.attachments.getValue();
    }
    
    public void setAttachments(ObservableList<File> files){
        this.attachments.set(files);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormBean other = (FormBean) obj;
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.cc, other.cc)) {
            return false;
        }
        if (!Objects.equals(this.bcc, other.bcc)) {
            return false;
        }
        if (!Objects.equals(this.subj, other.subj)) {
            return false;
        }
        return true;
    }

    
}
