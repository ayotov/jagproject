/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties;

import java.util.Objects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author grimm
 */
public class HTMLBean {
    private final StringProperty html;

    public HTMLBean(String html) {
        this.html = new SimpleStringProperty(html);
    }

    public String getHtml() {
        return html.get();
    }

    public void setHtml(String html) {
        this.html.set(html);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HTMLBean other = (HTMLBean) obj;
        if (!Objects.equals(this.html, other.html)) {
            return false;
        }
        return true;
    }
    
    
}
