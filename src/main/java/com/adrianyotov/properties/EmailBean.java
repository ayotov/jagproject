
package com.adrianyotov.properties;

import java.time.LocalDateTime;
import jodd.mail.Email;

/**
 *
 * @author Adrian Yotov
 */
public class EmailBean {
    private int id;
    private int folderID;
    private LocalDateTime sentDate;
    private LocalDateTime receivedDate;
    public Email email;
    
    public EmailBean(int id, int fID, LocalDateTime sendDate, LocalDateTime recDate, Email email){
        this.id = id;
        this.folderID = fID;
        this.sentDate = sendDate;
        this.receivedDate = recDate;
        this.email = email;
    }

    public EmailBean() {
        id = -1;
        folderID = -1;
        sentDate = null;
        receivedDate = null;
        email = new Email();
    }
    
    public int getID(){
        return this.id;
    }
    
    public int getFolderID(){
        return this.folderID;
    }
    
    public LocalDateTime getSendDate(){
        return this.sentDate;
    }
    
    public LocalDateTime getRecDate(){
        return this.receivedDate;
    }
    
    public void setID(int id){
        this.id = id;
    }
    
    public void setFolderID(int id){
        this.folderID = id;
    }
    
    public void setSentDate(LocalDateTime date){
        this.sentDate = date;
    }
    
    public void setRecDate(LocalDateTime date){
        this.receivedDate = date;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        EmailBean checker = (EmailBean) obj;
        if (!(id  == checker.getID() )){
            return false;
        }
        if (!(folderID == checker.getFolderID())){
            return false;
        }
        if (!(sentDate.equals(checker.getSendDate()))){
            return false;
        }
        if (!(receivedDate.equals(checker.getRecDate()))){
            return false;
        }
        if (!(email.equals(checker.email))){
            return false;
        }
        return true;
    }
}
