/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties.propertiesmanager;

import com.adrianyotov.properties.MailConfigBean;
import com.adrianyotov.properties.PropertyBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads and saves properties files
 * Also does checks for missing property fields
 * 
 * @author Kenneth Fogel, edited by Adrian Yotov
 */
public class PropertiesManager {

    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    /**
     * Updates a PropertyBean object with the contents of the properties file
     *
     * @param propertyBean
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final MailConfigBean propertyBean, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try ( InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }

                propertyBean.setUserName(prop.getProperty("userName"));
                propertyBean.setUserEmailAddress(prop.getProperty("emailAddress"));
                propertyBean.setMailPassword(prop.getProperty("mailPassword"));
                propertyBean.setUrlIMAP(prop.getProperty("imapURL"));
                propertyBean.setUrlSMTP(prop.getProperty("smtpURL"));
                propertyBean.setImapPort(prop.getProperty("imapPort"));
                propertyBean.setSmtpPort(prop.getProperty("smtpPort"));
                propertyBean.setMysqlURL(prop.getProperty("mysqlURL"));
                propertyBean.setMysqlDatabase(prop.getProperty("mysqlDatabase"));
                propertyBean.setMysqlPort(prop.getProperty("mysqlPort"));
                propertyBean.setDbUserName(prop.getProperty("mysqlUser"));
                propertyBean.setDbPassword(prop.getProperty("mysqlPassword"));

                found = correctPropertiesExist(prop);
            
        }
        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param propertyBean The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final MailConfigBean propertyBean) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("userName", propertyBean.getUserName());
        prop.setProperty("emailAddress", propertyBean.getUserEmailAddress());
        prop.setProperty("mailPassword", propertyBean.getMailPassword());
        prop.setProperty("imapURL", propertyBean.getUrlIMAP());
        prop.setProperty("smtpURL", propertyBean.getUrlSMTP());
        prop.setProperty("imapPort", propertyBean.getImapPort());
        prop.setProperty("smtpPort", propertyBean.getSmtpPort());
        prop.setProperty("mysqlURL", propertyBean.getUrlDB());
        prop.setProperty("mysqlDatabase", propertyBean.getMysqlDatabase());
        prop.setProperty("mysqlPort", propertyBean.getMysqlPort());
        prop.setProperty("mysqlUser", propertyBean.getDbUserName());
        prop.setProperty("mysqlPassword", propertyBean.getDbPassword());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try ( OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "SMTP Properties");
        }

    }

    /**
     * Helper that checks if all fields of a properties file are
     * Filled to ensure functionality with the database
     *
     * @param prop
     * @return True if properties aren't missing, false otherwise
     * @throws IOException
     */
    private boolean correctPropertiesExist(final Properties prop) throws IOException {

        if (prop.getProperty("userName").equals(null) || prop.getProperty("userName").equals("")) {
            return false;
        }

        if (prop.getProperty("emailAddress").equals(null) || prop.getProperty("emailAddress").equals("")) {
            return false;
        }

        if (prop.getProperty("mailPassword").equals(null) || prop.getProperty("mailPassword").equals("")) {

        }

        if (prop.getProperty("imapURL").equals(null) || prop.getProperty("imapURL").equals("")) {
            return false;
        }

        if (prop.getProperty("smtpURL").equals(null) || prop.getProperty("smtpURL").equals("")) {
            return false;
        }

        if (prop.getProperty("imapPort").equals(null) || prop.getProperty("imapPort").equals("")) {
            return false;
        }

        if (prop.getProperty("smtpPort").equals(null) || prop.getProperty("smtpPort").equals("")) {
            return false;
        }

        if (prop.getProperty("mysqlURL").equals(null) || prop.getProperty("mysqlURL").equals("")) {
            return false;
        }

        if (prop.getProperty("mysqlDatabase").equals(null) || prop.getProperty("mysqlDatabase").equals("")) {
            return false;
        }

        if (prop.getProperty("mysqlPort").equals(null) || prop.getProperty("mysqlPort").equals("")) {
            return false;
        }

        if (prop.getProperty("mysqlUser").equals(null) || prop.getProperty("mysqlUser").equals("")) {
            return false;
        }

        if (prop.getProperty("mysqlPassword").equals(null) || prop.getProperty("mysqlPassword").equals("")) {
            return false;
        }

        return true;
    }
}
