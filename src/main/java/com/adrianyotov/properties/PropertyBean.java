/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.properties;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kenneth Fogel, edited by Adrian Yotov
 */
public class PropertyBean {
    StringProperty userName;
    StringProperty userEmailAddress;
    StringProperty userEmailPassword;
    StringProperty urlIMAP;
    StringProperty urlSMTP;
    StringProperty portIMAP;
    StringProperty portSMTP;
    StringProperty urlDB;
    StringProperty dbName;
    StringProperty portDB;
    StringProperty dbUserName;
    StringProperty dbPassword;

    public PropertyBean(String userName, String emailAddress, String mailPassword,
            String imapURL, String smtpURL, String imapPort,
            String smtpPort, String mysqlURL, String mysqlDatabase,
            String mysqlPort, String mysqlUser, String mysqlPassword) {
        this.userName = new SimpleStringProperty(userName);
        this.userEmailAddress = new SimpleStringProperty(emailAddress);
        this.userEmailPassword = new SimpleStringProperty(mailPassword);
        this.urlIMAP = new SimpleStringProperty(imapURL);
        this.urlSMTP = new SimpleStringProperty(smtpURL);
        this.portIMAP = new SimpleStringProperty(imapPort);
        this.portSMTP = new SimpleStringProperty(smtpPort);
        this.urlDB = new SimpleStringProperty(mysqlURL);
        this.dbName = new SimpleStringProperty(mysqlDatabase);
        this.portDB = new SimpleStringProperty(mysqlPort);
        this.dbUserName = new SimpleStringProperty(mysqlUser);
        this.dbPassword = new SimpleStringProperty(mysqlPassword);
    }
    public PropertyBean() {
        this("", "", "", "", "", "", "", "", "", "", "", "");
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public String getEmailAddress() {
        return userEmailAddress.get();
    }

    public void setEmailAddress(String emailAddress) {
        this.userEmailAddress.set(emailAddress);
    }

    public StringProperty emailAddressProperty() {
        return userEmailAddress;
    }

    public String getMailPassword() {
        return userEmailPassword.get();
    }

    public void setMailPassword(String mailPassword) {
        this.userEmailPassword.set(mailPassword);
    }

    public StringProperty mailPasswordProperty() {
        return userEmailPassword;
    }

    public String getImapURL() {
        return urlIMAP.get();
    }

    public void setImapURL(String imapURL) {
        this.urlIMAP.set(imapURL);
    }

    public StringProperty imapURLProperty() {
        return urlIMAP;
    }

    public String getSmtpURL() {
        return urlSMTP.get();
    }

    public void setSmtpURL(String smtpURL) {
        this.urlSMTP.set(smtpURL);
    }

    public StringProperty smtpURLProperty() {
        return urlSMTP;
    }

    public String getImapPort() {
        return portIMAP.get();
    }

    public void setImapPort(String imapPort) {
        this.portIMAP.set(imapPort);
    }

    public StringProperty imapPortProperty() {
        return portIMAP;
    }

    public String getSmtpPort() {
        return portSMTP.get();
    }

    public void setSmtpPort(String smtpPort) {
        this.portSMTP.set(smtpPort);
    }

    public StringProperty smtpPortProperty() {
        return portSMTP;
    }

    public String getMysqlURL() {
        return urlDB.get();
    }

    public void setMysqlURL(String mysqlURL) {
        this.urlDB.set(mysqlURL);
    }

    public StringProperty mysqlURLProperty() {
        return urlDB;
    }

    public String getMysqlDatabase() {
        return dbName.get();
    }

    public void setMysqlDatabase(String mysqlDatabase) {
        this.dbName.set(mysqlDatabase);
    }

    public StringProperty mysqlDatabaseProperty() {
        return dbName;
    }

    public String getMysqlPort() {
        return portDB.get();
    }

    public void setMysqlPort(String mysqlPort) {
        this.portDB.set(mysqlPort);
    }

    public StringProperty mysqlPortProperty() {
        return portDB;
    }

    public String getMysqlUser() {
        return dbUserName.get();
    }

    public void setMysqlUser(String mysqlUser) {
        this.dbUserName.set(mysqlUser);
    }

    public StringProperty mysqlUserProperty() {
        return dbUserName;
    }

    public String getMysqlPassword() {
        return dbPassword.get();
    }

    public void setMysqlPassword(String mysqlPassword) {
        this.dbPassword.set(mysqlPassword);
    }

    public StringProperty mysqlPasswordProperty() {
        return dbPassword;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyBean other = (PropertyBean) obj;
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.userEmailAddress, other.userEmailAddress)) {
            return false;
        }
        if (!Objects.equals(this.userEmailPassword, other.userEmailPassword)) {
            return false;
        }
        if (!Objects.equals(this.urlIMAP, other.urlIMAP)) {
            return false;
        }
        if (!Objects.equals(this.urlSMTP, other.urlSMTP)) {
            return false;
        }
        if (!Objects.equals(this.portIMAP, other.portIMAP)) {
            return false;
        }
        if (!Objects.equals(this.portSMTP, other.portSMTP)) {
            return false;
        }
        if (!Objects.equals(this.urlDB, other.urlDB)) {
            return false;
        }
        if (!Objects.equals(this.dbName, other.dbName)) {
            return false;
        }
        if (!Objects.equals(this.portDB, other.portDB)) {
            return false;
        }
        if (!Objects.equals(this.dbUserName, other.dbUserName)) {
            return false;
        }
        if (!Objects.equals(this.dbPassword, other.dbPassword)) {
            return false;
        }
        return true;
    }
}
