/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.business;

import com.adrianyotov.properties.MailConfigBean;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.activation.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Flags;

import jodd.mail.EmailFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * This is jodd_mail_demo MainApp refactored to work as a business class rather
 * than a Main application Removed System.out.println with LOG.info Added an
 * HTML section to sendEmail()
 *
 * @author Ken, Adrian Yotov
 * @version 2.1
 *
 */
public class SendAndReceive {

    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);

    /**
     *
     * Standard send routine using Jodd.Jodd knows about GMail so no need to
 include port information
     * @param sender
     * @param toList
     * @param ccList
     * @param bccList
     * @param regAtt
     * @param embAtt
     * @param subject
     * @param message
     * @param HTMLText
     * @return 
     * @throws java.lang.Exception 
     */
    public Email sendEmail(MailConfigBean sender, ArrayList<String> toList, ArrayList<String> ccList, ArrayList<String> bccList,
            ArrayList<File> regAtt, ArrayList<String> embAtt, String subject, String message, String HTMLText) throws Exception {
        if (sender == null || !(checkEmail(sender.getUserEmailAddress()))) {
            throw new Exception("Sender email address is incorrect: " + sender.getUserEmailAddress());
        }

        Email email = Email.create().from(sender.getUserEmailAddress());
        if (((toList.isEmpty() && ccList.isEmpty() && bccList.isEmpty()) || (toList == null && ccList == null && bccList == null))) {
            LOG.error("There is no receiver");
            throw new Exception("There are no receivers");
        }
        if (checkEmail(sender.getUserEmailAddress())) {
            // Create am SMTP server object
            SmtpServer smtpServer = MailServer.create()
                    .ssl(true)
                    .host(sender.getUrlSMTP())
                    .auth(sender.getUserEmailAddress(), sender.getMailPassword())
                    //.debugMode(true)
                    .buildSmtpMailServer();

            email.subject(nullToEmpty(subject))
                    .textMessage(nullToEmpty(message))
                    .htmlMessage(nullToEmpty(HTMLText));

            // Checking recipient lists and attachments then adding them to the email
            if (!(toList.isEmpty() || toList == null)) {
                email = loadTo(email, toList);
            }

            if (!(ccList.isEmpty() || ccList == null)) {
                email = loadCC(email, ccList);
            }

            if (!(bccList.isEmpty() || bccList == null)) {
                email = loadBCC(email, bccList);
            }
            if (regAtt != null) {
                email = loadRegAttach(email, regAtt);
            }
            if (embAtt != null) {
                email = loadEmbAttach(email, embAtt);
            }

            // Like a file we open the session, send the message and close the
            // session
            try ( // A session is the object responsible for communicating with the server
                     SendMailSession session = smtpServer.createSession()) {
                // Like a file we open the session, send the message and close the
                // session
                session.open();
                session.sendMail(email);
                LOG.info("Email sent");
            }
        } else {
            throw new Exception("Something else went wrong");
        }
        return email;
    }
    
    /**
     * Creates a drafted email rather than sending it.
     * To be used for saving to the database
     * 
     * @param sender
     * @param toList
     * @param ccList
     * @param bccList
     * @param regAtt
     * @param embAtt
     * @param subject
     * @param message
     * @param HTMLText
     * @return the Email object 
     * @throws Exception 
     */
    public Email draftEmail(MailConfigBean sender, ArrayList<String> toList, ArrayList<String> ccList, ArrayList<String> bccList,
                            ArrayList<File> regAtt, ArrayList<String> embAtt, String subject, String message, String HTMLText) throws Exception{
        Email toDraft = Email.create().from(sender.getUserEmailAddress());
        if (!(toList.isEmpty())){
            toDraft = loadTo(toDraft, toList);
        }
        if (!(ccList.isEmpty())){
            toDraft = loadCC(toDraft, toList);
        }
        if (!(bccList.isEmpty())){
            toDraft = loadBCC(toDraft, toList);
        }
        if (regAtt != null){
            toDraft = loadRegAttach(toDraft, regAtt);
        }
        if (embAtt != null){
            toDraft = loadEmbAttach(toDraft, embAtt);
        }
        toDraft.subject(nullToEmpty(subject))
                    .textMessage(nullToEmpty(message))
                    .htmlMessage(nullToEmpty(HTMLText));
        
        
        return toDraft;
    }

    /**
     * Adds the "to" recipients after checking if the emails are correct
     *
     * @param email
     * @param toList
     * @return the original email object with the "to" recipients added
     * @throws Exception
     */
    private Email loadTo(Email email, ArrayList<String> toList) throws Exception {
        for (String to : toList) {
            if (checkEmail(to)) {
                email.to(to);
            } else {
                throw new Exception("Incorrect email: " + to);
            }
        }
        return email;
    }

    /**
     * Adds the "cc" recipients after checking if the emails are correct
     *
     * @param email
     * @param ccList
     * @return the original email object with the cc's added
     * @throws Exception
     */
    private Email loadCC(Email email, ArrayList<String> ccList) throws Exception {
        for (String cc : ccList) {
            if (checkEmail(cc)) {
                email.cc(cc);
            } else {
                throw new Exception("Incorrect email: " + cc);
            }
        }
        return email;
    }

    /**
     * Adds the "bcc" recipients after checking if the emails are correct
     *
     * @param email
     * @param bccList
     * @return the original email with the bcc's added
     * @throws Exception
     */
    private Email loadBCC(Email email, ArrayList<String> bccList) throws Exception {
        for (String bcc : bccList) {
            if (checkEmail(bcc)) {
                email.bcc(bcc);
            } else {
                throw new Exception("Incorrect email: " + bcc);
            }
        }
        return email;
    }

    /**
     * Adds regular attachments to the email
     *
     * @param email
     * @param attachments
     * @return the original email with regular attachments added
     */
    private Email loadRegAttach(Email email, ArrayList<File> attachments) {
        for (File reg : attachments) {
            email.attachment(EmailAttachment.with().content(reg));
        }
        return email;
    }

    /**
     * Adds embedded attachments to the email
     *
     * @param email
     * @param embAttach
     * @return the original email with embedded attachments added
     */
    private Email loadEmbAttach(Email email, ArrayList<String> embAttach) {
        for (String emb : embAttach) {
            email.embeddedAttachment(EmailAttachment.with().content(emb));
        }
        return email;
    }

    /**
     * Standard receive routine for Jodd using an ImapServer. Assumes the
     * existence of a folder c:\temp
     */
    public ReceivedEmail[] receiveEmail(MailConfigBean receiver) {
        if (checkEmail(receiver.getUserEmailAddress())) {
            ImapServer imapServer = MailServer.create()
                    .host(receiver.getUrlIMAP())
                    .ssl(true)
                    .auth(receiver.getUserEmailAddress(), receiver.getMailPassword())
                    //.debugMode(true)
                    .buildImapMailServer();

            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                if (emails != null) {
                    for (ReceivedEmail email : emails) {

                        // process messages
                        List<EmailMessage> messages = email.messages();

                        messages.stream().map((msg) -> {
                            return msg;
                        }).map((msg) -> {
                            return msg;
                        }).map((msg) -> {
                            return msg;
                        }).forEachOrdered((msg) -> {
                        });
                    }
                    return emails;
                }
            }
        } else {
            LOG.info("Unable to send email because either send or recieve addresses are invalid");
            return new ReceivedEmail[0];
        }
        return new ReceivedEmail[0];
    }


    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * Used to change null strings to empty strings To avoid NullPointer
     * Exceptions just in case
     *
     * @param s
     * @return the null string to an empty string
     */
    private String nullToEmpty(String s) {
        if (s == null) {
            return s = "";
        }
        return s;
    }

}
