package com.adrianyotov.fxcontroller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

/**
 * FXML Controller for the Email Form with recipients and email subject
 *
 * @author Adrian Yotov
 */
public class EmailFormFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="formTop"
    private GridPane formTop; // Value injected by FXMLLoader

    @FXML // fx:id="toField"
    private RowConstraints toField; // Value injected by FXMLLoader

    @FXML // fx:id="subjInput"
    private TextField subjInput; // Value injected by FXMLLoader

    @FXML // fx:id="toInput"
    private TextField toInput; // Value injected by FXMLLoader

    @FXML // fx:id="ccInput"
    private TextField ccInput; // Value injected by FXMLLoader

    @FXML // fx:id="bccInput"
    private TextField bccInput; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert formTop != null : "fx:id=\"formTop\" was not injected: check your FXML file 'emailFormFXML.fxml'.";
        assert toField != null : "fx:id=\"toField\" was not injected: check your FXML file 'emailFormFXML.fxml'.";
        assert subjInput != null : "fx:id=\"subjInput\" was not injected: check your FXML file 'emailFormFXML.fxml'.";
        assert toInput != null : "fx:id=\"toInput\" was not injected: check your FXML file 'emailFormFXML.fxml'.";
        assert ccInput != null : "fx:id=\"ccInput\" was not injected: check your FXML file 'emailFormFXML.fxml'.";
        assert bccInput != null : "fx:id=\"bccInput\" was not injected: check your FXML file 'emailFormFXML.fxml'.";

    }

    /**
     * Sets the form's recipients and disables editing of elements Since it is
     * meant to be vioewing an email, not editing it
     *
     * @param receivers
     * @param type
     */
    public void setReceivers(List<String> receivers, String type) {
        if (type.equals("to")) {
            setTo(receivers);
        }

        if (type.equals("cc")) {
            setCC(receivers);
        }

        if (type.equals("bcc")) {
            setBCC(receivers);
        }
        toInput.setDisable(true);
        ccInput.setDisable(true);
        bccInput.setDisable(true);
    }

    public void setSubject(String subject) {
        this.subjInput.setText(subject);
        this.subjInput.setDisable(true);
    }

    private void setTo(List<String> receivers) {
        StringBuilder sb = new StringBuilder();
        for (String to : receivers) {

            sb.append(to);

            if (to.equals(receivers.get(receivers.size() - 1))) {
                break;
            } else {

                sb.append(", ");

            }
        }
        this.toInput.setText(sb.toString());
    }

    private void setCC(List<String> receivers) {
        StringBuilder sb = new StringBuilder();
        for (String cc : receivers) {

            sb.append(cc);

            if (cc.equals(receivers.get(receivers.size() - 1))) {
                break;
            } else {

                sb.append(", ");

            }
        }
        this.ccInput.setText(sb.toString());
    }

    private void setBCC(List<String> receivers) {
        StringBuilder sb = new StringBuilder();
        for (String bcc : receivers) {

            sb.append(bcc);

            if (bcc.equals(receivers.get(receivers.size() - 1))) {
                break;
            } else {

                sb.append(", ");

            }
        }
        this.bccInput.setText(sb.toString());
    }

    /**
     * Clears the form and re-enables editing Meant as a "compose" button
     *
     */
    public void clearReceivers() {
        this.toInput.setText("");
        this.ccInput.setText("");
        this.bccInput.setText("");
        this.toInput.setDisable(false);
        this.ccInput.setDisable(false);
        this.bccInput.setDisable(false);
    }

    /**
     * Forces a selected email to re-enable editing Used only when a selected
     * email is in the draft folder
     *
     */
    public void removeRestrictions() {
        this.toInput.setDisable(false);
        this.ccInput.setDisable(false);
        this.bccInput.setDisable(false);
        this.subjInput.setDisable(false);
    }

    public void clearSubject() {
        this.subjInput.setText("");
        this.subjInput.setDisable(false);

    }

    public ArrayList<String> getTo() {
        ArrayList<String> toReturn = new ArrayList<>();
        String to = this.toInput.getText();
        if (to.replace(" ", "").isEmpty()) {
            return new ArrayList<>();
        } else {
            toReturn.addAll(Arrays.asList(to.split(",", 0)));
            return toReturn;
        }
    }

    public ArrayList<String> getCC() {
        ArrayList<String> toReturn = new ArrayList<>();
        String cc = this.ccInput.getText();
        if (cc.replace(" ", "").isEmpty()) {
            return new ArrayList<>();
        } else {
            toReturn.addAll(Arrays.asList(cc.split(",", 0)));
            return toReturn;
        }
    }

    public ArrayList<String> getBCC() {
        ArrayList<String> toReturn = new ArrayList<>();
        String bcc = this.bccInput.getText();
        if (bcc.replace(" ", "").isEmpty()) {
            return new ArrayList<>();
        } else {
            toReturn.addAll(Arrays.asList(bcc.split(",", 0)));
            return toReturn;
        }

    }

    public String getSubject() {
        return this.subjInput.getText();
    }
}
