package com.adrianyotov.fxcontroller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AboutController {

    private final static Logger LOG = LoggerFactory.getLogger(AboutController.class);
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private WebView aboutView;

    @FXML
    void initialize() {
        assert aboutView != null : "fx:id=\"aboutView\" was not injected: check your FXML file 'AboutFXML.fxml'.";
        
        final String html = "about.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
        
        aboutView.getEngine().load(uri.toString());
        LOG.info("Launching webview with URI: " + uri.toString());
                

    }
}