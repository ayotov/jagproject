package com.adrianyotov.fxcontroller;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;

/**
 * FXML Controller for the HTML Editor used for email messages
 *
 * @author Adrian Yotov
 */
public class HTMLEditorFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="htmlPane"
    private BorderPane htmlPane; // Value injected by FXMLLoader

    @FXML // fx:id="htmlEditor"
    private HTMLEditor htmlEditor; // Value injected by FXMLLoader

    private ArrayList<File> attachments;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert htmlPane != null : "fx:id=\"htmlPane\" was not injected: check your FXML file 'HTMLEditorFXMLController.fxml'.";
        assert htmlEditor != null : "fx:id=\"htmlEditor\" was not injected: check your FXML file 'HTMLEditorFXMLController.fxml'.";
        attachments = new ArrayList<>();

    }

    public void setHTMLText(String msg) {
        htmlEditor.setHtmlText(msg);
        htmlEditor.setDisable(true);
    }

    public void clearText() {
        htmlEditor.setHtmlText("");
        htmlEditor.setDisable(false);
    }

    public String getHTMLText() {
        return this.htmlEditor.getHtmlText();
    }

    public void removeRestrictions() {
        this.htmlEditor.setDisable(false);
    }

    public void addFileToHTML(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.htmlEditor.getHtmlText());
        String uri = file.getName();
        
        String tag = ("<img src=\"./"+ uri + "\">");
        this.htmlEditor.setHtmlText(tag);
    }

    public void insertAttachment(File file) {
        attachments.add(file);
    }

    public ArrayList<File> getAttachments() {
        return this.attachments;
    }
}
