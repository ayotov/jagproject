package com.adrianyotov.fxcontroller;

import com.adrianyotov.business.SendAndReceive;
import com.adrianyotov.jagproject.MainAppFX;
import com.adrianyotov.properties.EmailBean;
import com.adrianyotov.properties.FormBean;
import com.adrianyotov.properties.MailConfigBean;
import com.adrianyotov.properties.propertiesmanager.PropertiesManager;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Array;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.ReceivedEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.EmailDAO;
import persistence.EmailDAOImpl;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import javafx.stage.FileChooser;
import javax.activation.DataSource;

/**
 * FXML Controller for the Root Layout
 *
 * @author Adrian Yotov
 */
public class RootFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(RootFXMLController.class);

    ConfigFXMLController config;
    MailConfigBean bean;

    @FXML // fx:id="openConfig"
    private MenuItem openConfig; // Value injected by FXMLLoader

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // fx:id="insertBtn"
    private MenuItem insertBtn; // Value injected by FXMLLoader

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="leftSplit"
    private BorderPane leftSplit; // Value injected by FXMLLoader

    @FXML // fx:id="upperRightSplit"
    private AnchorPane upperRightSplit; // Value injected by FXMLLoader

    @FXML // fx:id="bottomRightSplit"
    private BorderPane bottomRightSplit; // Value injected by FXMLLoader

    @FXML
    void onInsertAttach(MouseEvent event) {
        FileChooser chooser = new FileChooser();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);

        File selected = chooser.showOpenDialog(stage);

        this.htmlEditorController.addFileToHTML(selected);
        this.htmlEditorController.insertAttachment(selected);
    }

    @FXML
    void openAbout(ActionEvent event) throws IOException {
        AboutController about = new AboutController();
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resources);
        loader.setLocation(RootFXMLController.class.getResource("/fxml/AboutFXML.fxml"));
        Parent root = loader.load();
        about = loader.getController();

        stage.setScene(new Scene(root));
        stage.setTitle(resources.getString("help"));
        stage.getIcons().add(
                new Image(MainAppFX.class
                        .getResourceAsStream("/images/cog.png")));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    @FXML
    void onClickConfig(ActionEvent event) throws IOException, SQLException {
        showConfig();
        bean = setConfig();
        setDAO(bean);
        emailTableFXController.setEmailDAO(emailDAO);
        treeFXController.setEmailDAO(emailDAO);
        setTableControllerToTree();
        this.emailTableFXController.setRootController(this);
        try {
            treeFXController.displayTree();
        } catch (SQLException e) {
            errorAlert(e.getMessage());
            showConfig();
            this.initialize();
        }
        emailTableFXController.displayTable(FolderType.INBOX.getId());

    }

    @FXML
    void onClear(MouseEvent event) {
        this.htmlEditorController.clearText();
        this.formFXController.clearReceivers();
        this.formFXController.clearSubject();
    }

    /**
     * Obtains all received emails that aren't in the database Meant to be used
     * as a refresh button
     *
     * @param event
     * @throws SQLException
     * @throws IOException
     */
    @FXML
    void onReceive(MouseEvent event) throws SQLException, IOException {
        try {
            ReceivedEmail[] received = mailMan.receiveEmail(bean);
            LOG.info("Emails found: " + received.length);
            for (ReceivedEmail toChange : received) {
                Email newMail = this.convertReceivedEmail(toChange);
                EmailBean toAdd = new EmailBean(0, FolderType.INBOX.getId(), LocalDateTime.now(), LocalDateTime.now(), newMail);
                emailDAO.createEmail(toAdd);
            }
            emailTableFXController.displayTable(FolderType.INBOX.getId());
        } catch (IOException | SQLException e) {
            errorAlert(e.getMessage());
        }

    }

    /**
     * Sends an email using text from the form and HTML Editor Does validation
     * like usual
     *
     * @param event
     * @throws SQLException
     * @throws IOException
     */
    @FXML
    void onSend(MouseEvent event) throws SQLException, IOException {
        boolean sent = false;
        ArrayList<String> to = formFXController.getTo();
        LOG.info(to.toString());
        ArrayList<String> cc = formFXController.getCC();
        LOG.info(cc.toString());
        ArrayList<String> bcc = formFXController.getBCC();
        LOG.info(bcc.toString());
        ArrayList<File> attachments = this.htmlEditorController.getAttachments();

        String subject = obtainSubject();
        String htmlText = obtainHTMLText();
        try {
            EmailBean toCreate;
            Email email = mailMan.sendEmail(bean, to, cc, bcc, attachments, null, subject, "", htmlText);
            
            toCreate = new EmailBean(0, FolderType.SENT.getId(), LocalDateTime.now(), null, email);
            if (emailTableFXController.draftSelected()){
                EmailBean toUpdate = new EmailBean(emailTableFXController.getEmailId(), FolderType.SENT.getId(), LocalDateTime.now(), null, email);
                emailDAO.updateDraft(toUpdate);
                emailDAO.updateEmailToFolder(toUpdate.getID(), "Sent");
            }
            else {
                emailDAO.createEmail(toCreate);

            }
            sent = true;
        } catch (Exception e) {
            errorAlert(e.getMessage());
        }
        if (sent) {
            infoAlert(resources.getString("sent"));
            emailTableFXController.displayTable(FolderType.SENT.getId());
        }
    }

    /**
     * Functions the same as Sending an Email but has no validation checks since
     * Saves as a draft
     *
     * @param event
     */
    @FXML
    void onSaveDraft(MouseEvent event) {
        ArrayList<String> to = formFXController.getTo();
        LOG.info(to.toString());
        ArrayList<String> cc = formFXController.getCC();
        LOG.info(cc.toString());
        ArrayList<String> bcc = formFXController.getBCC();
        LOG.info(bcc.toString());

        String subject = obtainSubject();
        String htmlText = obtainHTMLText();
        try {
            if (emailTableFXController.draftSelected()) {
                Email email = mailMan.draftEmail(bean, to, cc, bcc, null, null, subject, "", htmlText);
                EmailBean toUpdate = new EmailBean(emailTableFXController.getEmailId(), FolderType.DRAFT.getId(), null, null, email);
                emailDAO.updateDraft(toUpdate);
                emailTableFXController.displayTable(FolderType.DRAFT.getId());

            } else {
                Email email = mailMan.draftEmail(bean, to, cc, bcc, null, null, subject, "", htmlText);
                EmailBean toCreate = new EmailBean(0, FolderType.DRAFT.getId(), null, null, email);
                emailDAO.createEmail(toCreate);
                emailTableFXController.displayTable(FolderType.DRAFT.getId());
            }
        } catch (Exception e) {
            errorAlert(e.getMessage());
        }
    }

    private SendAndReceive mailMan;
    private EmailDAO emailDAO;
    private TreeFXMLController treeFXController;
    private TableFXMLController emailTableFXController;
    private EmailFormFXMLController formFXController;
    private HTMLEditorFXMLController htmlEditorController;

    public RootFXMLController() throws IOException {
        this.bean = setConfig();
        this.emailDAO = new EmailDAOImpl(bean);
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException, SQLException {
        mailMan = new SendAndReceive();
        initLeftPane();
        initTopRightPane();
        initBottomRightPane();
        setTableControllerToTree();
        setDAO(bean);
        this.emailTableFXController.setRootController(this);
        try {
            treeFXController.displayTree();
        } catch (SQLException e) {
            errorAlert(e.getMessage());
            showConfig();
            this.initialize();
        }
        emailTableFXController.displayTable(FolderType.INBOX.getId());
        this.bean = setConfig();

    }

    /**
     * Initializes the Split BorderPane on the left side with the tree view of
     * folders Currently uses fake data
     *
     * @throws IOException
     */
    private void initLeftPane() throws IOException {
        try {

            LOG.info("Initializing left pane");
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootFXMLController.class.getResource("/fxml/treeFXML.fxml"));
            BorderPane treeView = (BorderPane) loader.load();
            treeFXController = loader.getController();
            treeFXController.setEmailDAO(emailDAO);
            leftSplit.getChildren().add(treeView);
            treeView.prefWidthProperty().bind(leftSplit.widthProperty());
            treeView.prefHeightProperty().bind(leftSplit.heightProperty());

        } catch (IOException ex) {
            LOG.error("Left Split error", ex);
            errorAlert("initLeftPane()");
            Platform.exit();
        }

    }

    private void setTableControllerToTree() {
        treeFXController.setTableController(emailTableFXController);
    }

    /**
     * Initializes the Right Split Pane on top where the table goes Currently
     * uses Fake Data
     *
     * @throws IOException
     */
    private void initTopRightPane() throws IOException {
        try {

            LOG.info("Initializing top right pane");
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(RootFXMLController.class.getResource("/fxml/tableFXML.fxml"));
            BorderPane tableView = (BorderPane) loader.load();

            emailTableFXController = loader.getController();
            emailTableFXController.setEmailDAO(emailDAO);
            upperRightSplit.getChildren().add(tableView);
            tableView.prefHeightProperty().bind(upperRightSplit.heightProperty());
            tableView.prefWidthProperty().bind(upperRightSplit.widthProperty());

        } catch (IOException ex) {
            LOG.error("Left Split error", ex);
            errorAlert("initLeftPane()");
            Platform.exit();

        }
    }

    /**
     * Initializes the Right Split Pane on the bottom where the HTML Editor As
     * well as the email Form where recipients and the subject reside
     *
     * @throws IOException
     */
    private void initBottomRightPane() throws IOException {
        try {

            LOG.info("Initializing bottom right ");
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(RootFXMLController.class.getResource("/fxml/emailFormFXML.fxml"));
            BorderPane htmlView = (BorderPane) loader.load();
            formFXController = loader.getController();
            htmlView.prefHeightProperty().bind(bottomRightSplit.heightProperty());
            htmlView.prefWidthProperty().bind(bottomRightSplit.widthProperty());

            FXMLLoader sndLoader = new FXMLLoader();
            sndLoader.setResources(resources);
            sndLoader.setLocation(RootFXMLController.class.getResource("/fxml/HTMLEditorFXML.fxml"));
            BorderPane htmlEditor = (BorderPane) sndLoader.load();
            htmlEditorController = sndLoader.getController();

            bottomRightSplit.getChildren().addAll(htmlEditor, htmlView);
            htmlEditor.prefHeightProperty().bind(bottomRightSplit.heightProperty());
            htmlEditor.prefWidthProperty().bind(bottomRightSplit.widthProperty());

        } catch (IOException ex) {
            LOG.error("Left Split error", ex);
            errorAlert("initLeftPane()");
            Platform.exit();
        }

    }

    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(msg);
        dialog.showAndWait();
    }

    private void infoAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(resources.getString("mailman"));
        alert.setHeaderText(resources.getString("sendingInfo"));
        alert.setContentText(msg);
        alert.showAndWait();
    }

    /**
     * Helper for the onAction method onClickConfig inside the menu items to
     * open up the Config form with the user and database details
     *
     * @throws IOException
     */
    private void showConfig() throws IOException {
        try {

            LOG.info("Showing config");
            bean = new MailConfigBean();
            PropertiesManager manager = new PropertiesManager();
            manager.loadTextProperties(bean, "", "MailConfig");
            Stage stage = new Stage();

            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(RootFXMLController.class.getResource("/fxml/ConfigFXML.fxml"));
            Parent root = loader.load();
            config = loader.getController();
            config.setUpProperties(manager, bean);

            stage.setScene(new Scene(root));
            stage.setTitle(resources.getString("Title"));
            stage.getIcons().add(
                    new Image(MainAppFX.class
                            .getResourceAsStream("/images/cog.png")));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

        } catch (IOException ex) {
            LOG.error("Left Split error", ex);
            errorAlert("showConfig()");
            Platform.exit();
        }

    }

    private void setDAO(MailConfigBean bean) {
        this.emailDAO = new EmailDAOImpl(bean);
    }

    /**
     * Sets up the MailConfigBean with information from the .properties file in
     * root
     *
     * @return
     * @throws IOException
     */
    private MailConfigBean setConfig() throws IOException {
        LOG.info("Checking if mailconfig.properties is valid");

        PropertiesManager mailManager = new PropertiesManager();
        MailConfigBean mailConfig = new MailConfigBean();
        
        boolean validConfig = false;
        try {
            validConfig = mailManager.loadTextProperties(mailConfig, "", "mailconfig");
        } catch (IOException ex) {
            LOG.error("Something went wrong in mailconfig.properties", ex);
            errorAlert("fileError");
            Platform.exit();
        }

        //If file is not valid, show the settings window
        if (!validConfig) {
            showConfig();
        }
        return mailConfig;

    }

    public void setHtmlText(String msg) {
        this.htmlEditorController.setHTMLText(msg);
    }

    public void setReceivers(List<String> to, List<String> cc, List<String> bcc) {
        this.formFXController.setReceivers(to, "to");
        this.formFXController.setReceivers(cc, "cc");
        this.formFXController.setReceivers(bcc, "bcc");
    }

    public void removeRestrictions() {
        this.formFXController.removeRestrictions();
        this.htmlEditorController.removeRestrictions();
    }

    public void setSubject(String subject) {
        this.formFXController.setSubject(subject);
    }

    public void clearReceivers() {
        this.formFXController.clearReceivers();
    }

    private String obtainSubject() {
        return formFXController.getSubject();
    }

    private String obtainHTMLText() {
        return htmlEditorController.getHTMLText();
    }

    /**
     * Converts a ReceivedEmail into an Email used for storing into the database
     *
     * @param email
     * @return the converted Email
     */
    private Email convertReceivedEmail(ReceivedEmail email) {
        Email toReturn = Email.create().from(email.from())
                .subject(email.subject())
                .textMessage(email.messages().get(0).getContent())
                .htmlMessage(email.messages().get(1).getContent())
                .to(email.to())
                .cc(email.cc());

        List<EmailAttachment<? extends DataSource>> attachments = email.attachments();
        for (EmailAttachment att : attachments){
            if (att.getContentId() != null){
                att.writeToFile(new File(att.getName()));
                toReturn.embeddedAttachment(EmailAttachment.with().content(new File(att.getName())));
            }
            else {
                toReturn.attachment(EmailAttachment.with().content(new File(att.getName())));
            }
        }
        return toReturn;
    }

}
