package com.adrianyotov.fxcontroller;

import com.adrianyotov.properties.EmailTableBean;
import com.adrianyotov.properties.FolderBean;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.EmailDAO;

/**
 * FXML Controller for the folder tree
 *
 * @author Adrian Yotov
 */
public class TreeFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML
    private Button addBtn;

    @FXML
    private TextField deleteText;

    @FXML // fx:id="tree"
    private TreeView<FolderBean> folderTree; // Value injected by FXMLLoader

    private final static Logger LOG = LoggerFactory.getLogger(TreeFXMLController.class);
    private EmailDAO emailDAO;
    private TableFXMLController emailTableFXController;

    @FXML
    void onDragDrop(DragEvent event) throws IOException {
        Dragboard db = event.getDragboard();
        boolean success = false;

        if (event.getDragboard().hasString()) {
            String text = db.getString();
            // Obtains the target's info
            String folderType = event.getTarget().toString();
            // Splits the previous string and obtain the Folder's Name
            String folderName = folderType.split("\"")[1];

            try {
                emailDAO.updateEmailToFolder(parseInt(text), folderName);
                success = true;
                LOG.info("Email " + text + " was moved to " + folderName);
                emailTableFXController.displayTable(emailDAO.getFolderID(folderName));
            } catch (SQLException e) {
                errorAlert(e.getMessage());
            }

        }
        event.setDropCompleted(success);
        event.consume();
    }

    @FXML
    void onDragOver(DragEvent event) {
        if (event.getGestureSource() != folderTree && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    @FXML
    void onDelete(MouseEvent event) throws SQLException {
        try {
            emailDAO.deleteFolder(deleteText.getText());
        } catch (SQLException e) {
            errorAlert(e.getMessage());
        }
        displayTree();
        deleteText.setText("");
    }

    @FXML
    void onAdd(MouseEvent event) throws SQLException {
        try {

            emailDAO.createFolder(folderText.getText());

        } catch (SQLException e) {

            errorAlert(e.getMessage());

        }
        displayTree();
        folderText.setText("");
    }

    public void setEmailDAO(EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    public void setTableController(TableFXMLController controller) {
        emailTableFXController = controller;
    }

    @FXML
    private TextField folderText;

    /**
     * Displays emails inside a folder by using their specific folder key
     *
     * @throws java.sql.SQLException
     */
    public void displayTree() throws SQLException {
        folderTree.getRoot().getChildren().clear();
        ObservableList<FolderBean> folders = emailDAO.findFolders();
        if (folders != null) {
            folders.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                return item;
            }).forEachOrdered((item) -> {
                folderTree.getRoot().getChildren().add(item);
            });
        }
        folderTree.getRoot().setExpanded(true);
        folderTree.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    try {
                        showTreeDetails(newValue);
                    } catch (SQLException | IOException ex) {
                        java.util.logging.Logger.getLogger(TreeFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        FolderBean folder = new FolderBean();
        folder.setfName(resources.getString("folder"));
        folderTree.setRoot(new TreeItem<>(folder));
        folderTree.setCellFactory((e) -> new TreeCell<FolderBean>() {
            @Override
            protected void updateItem(FolderBean fd, boolean empty) {
                super.updateItem(fd, empty);
                if (fd != null) {
                    setText(fd.getfName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });

    }

    /**
     * Binding tree items (folders) to the emails that have the same ID As the
     * folders and then displaying those emails
     *
     * @param newValue
     * @throws SQLException
     */
    private void showTreeDetails(TreeItem<FolderBean> newValue) throws SQLException, IOException {
        TableView<EmailTableBean> emailTable = emailTableFXController.getEmailTable();

        FilteredList<EmailTableBean> folderMail = emailTable.getItems()
                .filtered(email -> email.getfID() == newValue.getValue().getId());
        emailTableFXController.displayTable(newValue.getValue().getId());
    }

    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(msg);
        dialog.show();
    }
}
