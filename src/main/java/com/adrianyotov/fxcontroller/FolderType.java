/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrianyotov.fxcontroller;

/**
 *
 * @author grimm
 */
public enum FolderType {
    INBOX(1),
    SENT(2),
    DRAFT(3);

    /**
     *
     */
    private final int id;

    private FolderType(int type) {
        this.id = type;
    }
    
    public int getId(){
        return this.id;
    }
}
