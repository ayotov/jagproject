package com.adrianyotov.fxcontroller;

/**
 * Sample Skeleton for 'tableFXML.fxml' Controller Class
 */
import com.adrianyotov.properties.EmailBean;
import com.adrianyotov.properties.EmailTableBean;
import com.adrianyotov.properties.MailConfigBean;
import com.adrianyotov.properties.propertiesmanager.PropertiesManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import jodd.mail.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.EmailDAO;

/**
 * FXML Controller for the table in the top right pane
 *
 * @author Adrian Yotov
 */
public class TableFXMLController {

    private RootFXMLController rootController;

    private final static Logger LOG = LoggerFactory.getLogger(TableFXMLController.class);

    private EmailDAO emailDAO;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // fx:id="EmailPane"
    private BorderPane EmailPane; // Value injected by FXMLLoader

    @FXML // fx:id="emailTable"
    private TableView<EmailTableBean> emailTable; // Value injected by FXMLLoader

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="fromCol"
    private TableColumn<EmailTableBean, String> fromCol; // Value injected by FXMLLoader

    @FXML // fx:id="subjCol"
    private TableColumn<EmailTableBean, String> subjCol; // Value injected by FXMLLoader

    @FXML
    private TableColumn<EmailTableBean, String> recDateCol;

    @FXML
    private TableColumn<EmailTableBean, String> sendDateCol;

    /**
     * Drag method that works only on Email Table items, if they are not null
     *
     * @param event
     */
    @FXML
    void onDragDetected(MouseEvent event) {
        String selected = "" + emailTable.getSelectionModel().getSelectedItem().getId();

        if (selected != null) {
            Dragboard db = emailTable.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString(selected);
            db.setContent(content);
            event.consume();
        }
    }

    /**
     * Deletes a folder through the database, if it is allowed or if the folder
     * exists
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void onDelete(MouseEvent event) throws IOException {
        EmailTableBean selected = emailTable.getSelectionModel().getSelectedItem();
        LOG.info("folder id is: " + selected.getfID());

        if (selected != null) {
            try {
                int fID = selected.getfID();
                emailDAO.deleteEmail(selected.getId());
                displayTable(fID);
            } catch (SQLException e) {
                errorAlert(e.getMessage());
            }
        }
    }

    /**
     * Saves regular attachments to the root folder if an email is selected in
     * the table
     *
     * @param event
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws IOException
     */
    @FXML
    void onSaveAttachments(MouseEvent event) throws SQLException, FileNotFoundException, IOException {
        EmailTableBean selected = emailTable.getSelectionModel().getSelectedItem();
        if (selected != null) {
            emailDAO.saveEmailAttachments(selected.getId());
        }

    }

    /**
     * Sets up a reply email to a sender if an email is selected and is not a
     * draft
     *
     * @param event
     * @throws SQLException
     */
    @FXML
    void onReply(MouseEvent event) throws SQLException {
        EmailTableBean selected = emailTable.getSelectionModel().getSelectedItem();
        if (selected.getfID() != FolderType.DRAFT.getId() && selected != null) {
            showReplyDetails(selected);
            rootController.removeRestrictions();
        }

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException, SQLException {

        fromCol.setCellValueFactory(cellData -> cellData.getValue().fromProperty());
        subjCol.setCellValueFactory(cellData -> cellData.getValue().subjProperty());
        recDateCol.setCellValueFactory(cellData -> cellData.getValue().recDateProperty());
        sendDateCol.setCellValueFactory(cellData -> cellData.getValue().sendDateProperty());

        adjustColWidths();
        emailTable.getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    try {
                        showEmailDetails(newValue);
                    } catch (SQLException ex) {
                        java.util.logging.Logger.getLogger(TableFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

    }

    public void setEmailDAO(EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    public void setRootController(RootFXMLController controller) {
        this.rootController = controller;
    }

    /**
     * Shows a folder's contained emails through the given id
     *
     * @param id
     * @throws SQLException
     * @throws java.io.IOException
     */
    public void displayTable(int id) throws SQLException, IOException {
        MailConfigBean bean = new MailConfigBean();
        PropertiesManager mng = new PropertiesManager();
        mng.loadTextProperties(bean, "", "MailConfig");
        ArrayList<EmailBean> toConvert = emailDAO.findInFolder(id);
        ObservableList<EmailTableBean> emails = FXCollections.observableArrayList();
        toConvert.forEach(mail -> {
            emails.add(convertEmailBean(mail));
        });
        emailTable.setItems(emails);
    }

    /**
     * Used in other classes to check if a selected email is a draft
     *
     * @return true if the email is a draft, false if it isn't
     */
    public boolean draftSelected() {
        if (emailTable.getSelectionModel().getSelectedItem().getfID() == FolderType.DRAFT.getId() && emailTable.getSelectionModel().getSelectedItem() != null) {
            return true;
        }
        return false;
    }

    public int getEmailId() {
        if (emailTable.getSelectionModel().getSelectedItem().getId() > 0 && emailTable.getSelectionModel().getSelectedItem() != null) {
            return emailTable.getSelectionModel().getSelectedItem().getId();
        } else {
            return 0;
        }
    }

    /**
     * Setting table widths and heights so they are dynamic
     *
     */
    private void adjustColWidths() {

        double width = EmailPane.getPrefWidth();
        fromCol.setPrefWidth(width * 0.15);
        subjCol.setPrefWidth(width * 0.60);
        recDateCol.setPrefWidth(width * 0.12);
        sendDateCol.setPrefWidth(width * 0.12);
    }

    /**
     * Populates the form and HTML editor with the selected email's data
     *
     * @param data
     * @throws SQLException
     */
    private void showEmailDetails(EmailTableBean data) throws SQLException {
        try {
            ArrayList<String> filenames = emailDAO.findEmailAttachmentNames(data.getId());
            String htmlAttach = addEmbeddedAttach(filenames);
            rootController.setHtmlText(htmlAttach + data.getHtmlMsg()  );
            ArrayList<String> toList = cleanStrings(data.getTo());
            ArrayList<String> ccList = cleanStrings(data.getCC());
            ArrayList<String> bccList = cleanStrings(data.getBCC());

            rootController.setReceivers(toList, ccList, bccList);
            rootController.setSubject(data.getSubj());
            if (data.getfID() == FolderType.DRAFT.getId()) {
                rootController.removeRestrictions();
            }
        } catch (NullPointerException e) {
            rootController.clearReceivers();
        }
    }

    /**
     * Returns an embedded attachment String to use in the HTML editor
     *
     * @param filenames
     * @return the image tag for the HTML editor
     */
    private String addEmbeddedAttach(ArrayList<String> filenames) {
        String sb = "";
        for (String name : filenames) {
            File file = new File(name);
            String uri = file.getName();
            LOG.info(sb);
            sb = sb + ("<img src=\"./" + uri + "\">");
        }
        LOG.info(sb);
        return sb;
    }

    /**
     * Populates the To field and adds "RE:" to the selected email's subject As
     * it is meant to be a reply to the sender's email
     *
     * @param data
     */
    private void showReplyDetails(EmailTableBean data) {

        List<String> receiver = new ArrayList<>();
        receiver.add(cleanString(data.getFrom()));
        // Setting CC and BCC as empty lists since this is simply a reply email
        rootController.setReceivers(receiver, new ArrayList<>(), new ArrayList<>());
        rootController.setSubject("RE: " + data.getSubj());

    }

    public TableView<EmailTableBean> getEmailTable() {
        return emailTable;
    }

    /**
     * Converts an EmailBean to an EmailTableBean to be used for the table
     * controller
     *
     * @param email
     * @return the converted EmailTableBean
     */
    private EmailTableBean convertEmailBean(EmailBean email) {
        ArrayList<String> sentTo = cleanStrings(getTo(email));
        ArrayList<String> sentCC = cleanStrings(getCC(email));
        ArrayList<String> sentBCC = cleanStrings(getBCC(email));
        EmailTableBean toReturn = (new EmailTableBean(email.getID(), email.getFolderID(),
                email.email.from().getEmail(), email.email.messages().get(0).getContent(),
                email.email.messages().get(1).getContent(), email.email.subject(), email.getRecDate(), email.getSendDate(), sentTo, sentCC, sentBCC));

        return toReturn;
    }

    private ArrayList<String> getTo(EmailBean mail) {
        ArrayList<String> toReturn = new ArrayList<>();
        if (mail.email.to().length != 0) {

            for (EmailAddress sent : mail.email.to()) {
                toReturn.add(sent.getEmail());
            }
        }
        return toReturn;

    }

    private ArrayList<String> getCC(EmailBean mail) {
        ArrayList<String> toReturn = new ArrayList<>();
        if (mail.email.cc().length != 0) {
            for (EmailAddress sent : mail.email.cc()) {
                toReturn.add(sent.getEmail());
            }
        }
        return toReturn;

    }

    private ArrayList<String> getBCC(EmailBean mail) {
        ArrayList<String> toReturn = new ArrayList<>();
        if (mail.email.bcc().length != 0) {

            for (EmailAddress sent : mail.email.bcc()) {
                toReturn.add(sent.getEmail());
            }
        }
        return toReturn;

    }

    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Cleans up recipient strings so they don't have square brackets in the
     * strings
     *
     * @param toClean
     * @return the cleaned recipients
     */
    private ArrayList<String> cleanStrings(ArrayList<String> toClean) {
        if (toClean != null) {
            ArrayList<String> toReturn = new ArrayList<>();
            for (String address : toClean) {
                toReturn.add(address.replace("[[", "").replace("]]", ""));
            }
            return toReturn;
        }
        return new ArrayList<>();
    }

    /**
     * Same as above except for single strings
     *
     * @param toClean
     * @return the cleaned String
     */
    private String cleanString(String toClean) {
        if (toClean != null || toClean.equals("")) {
            return toClean.replace("[", "").replace("]", "");
        }
        return "";
    }

}
