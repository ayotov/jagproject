package com.adrianyotov.fxcontroller;

import com.adrianyotov.properties.MailConfigBean;
import com.adrianyotov.properties.PropertyBean;
import com.adrianyotov.properties.propertiesmanager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller for the Config form
 * 
 * @author Adrian Yotov
 */
public class ConfigFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(ConfigFXMLController.class);
    private MailConfigBean bean;
    private PropertiesManager manager;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="nameField"
    private TextField nameField; // Value injected by FXMLLoader

    @FXML // fx:id="emailField"
    private TextField emailField; // Value injected by FXMLLoader

    @FXML // fx:id="emailPassField"
    private TextField emailPassField; // Value injected by FXMLLoader

    @FXML // fx:id="urlIMAPField"
    private TextField urlIMAPField; // Value injected by FXMLLoader

    @FXML // fx:id="urlSMTPField"
    private TextField urlSMTPField; // Value injected by FXMLLoader

    @FXML // fx:id="portIMAPField"
    private TextField portIMAPField; // Value injected by FXMLLoader

    @FXML // fx:id="portSMTPField"
    private TextField portSMTPField; // Value injected by FXMLLoader

    @FXML // fx:id="dbURLField"
    private TextField dbURLField; // Value injected by FXMLLoader

    @FXML // fx:id="dbNameField"
    private TextField dbNameField; // Value injected by FXMLLoader

    @FXML // fx:id="dbPortField"
    private TextField dbPortField; // Value injected by FXMLLoader

    @FXML // fx:id="dbUserField"
    private TextField dbUserField; // Value injected by FXMLLoader

    @FXML // fx:id="dbPassField"
    private TextField dbPassField; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML
    private Button cancelBtn;
       
    @FXML
    void onClickSave(MouseEvent event) throws IOException {
        
        LOG.info("Saving config info and closing");
        manager.writeTextProperties("", "MailConfig", bean);
        Stage stage = (Stage) saveBtn.getScene().getWindow();
        stage.close();
    }
    
    public void setUpProperties(PropertiesManager pm, MailConfigBean pb){
        bean = pb;
        manager = pm;
        doBindings();
    }
    
    @FXML
    void onCancelClick(MouseEvent event) {
        
        LOG.info("Closing config without saving");
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        stage.close();
     }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert nameField != null : "fx:id=\"nameField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert emailField != null : "fx:id=\"emailField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert emailPassField != null : "fx:id=\"emailPassField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert urlIMAPField != null : "fx:id=\"urlIMAPField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert urlSMTPField != null : "fx:id=\"urlSMTPField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert portIMAPField != null : "fx:id=\"portIMAPField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert portSMTPField != null : "fx:id=\"portSMTPField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert dbURLField != null : "fx:id=\"dbURLField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert dbNameField != null : "fx:id=\"dbNameField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert dbPortField != null : "fx:id=\"dbPortField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert dbUserField != null : "fx:id=\"dbUserField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert dbPassField != null : "fx:id=\"dbPassField\" was not injected: check your FXML file 'ConfigFXML.fxml'.";
        assert saveBtn != null : "fx:id=\"saveBtn\" was not injected: check your FXML file 'ConfigFXML.fxml'.";

    }
    
    
    /**
     * Binds all of a JavaFX EmailBean's properties to the actual Config
     * If something changes in a file or is saved from the program, both will change accordingly
     * 
     */
    private void doBindings(){
        Bindings.bindBidirectional(nameField.textProperty(), bean.userNameProperty());
        Bindings.bindBidirectional(emailField.textProperty(), bean.emailAddressProperty());
        Bindings.bindBidirectional(emailPassField.textProperty(), bean.mailPasswordProperty());
        Bindings.bindBidirectional(urlIMAPField.textProperty(), bean.imapURLProperty());
        Bindings.bindBidirectional(urlSMTPField.textProperty(), bean.smtpURLProperty());
        Bindings.bindBidirectional(portIMAPField.textProperty(), bean.imapPortProperty());
        Bindings.bindBidirectional(portSMTPField.textProperty(), bean.smtpPortProperty());
        Bindings.bindBidirectional(dbURLField.textProperty(), bean.mysqlURLProperty());
        Bindings.bindBidirectional(dbNameField.textProperty(), bean.mysqlDatabaseProperty());
        Bindings.bindBidirectional(dbPortField.textProperty(), bean.mysqlPortProperty());
        Bindings.bindBidirectional(dbUserField.textProperty(), bean.mysqlUserProperty());
        Bindings.bindBidirectional(dbPassField.textProperty(), bean.mysqlPasswordProperty());

    }
}