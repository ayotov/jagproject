
package com.adrianyotov.jagproject;

import com.adrianyotov.fxcontroller.ConfigFXMLController;
import com.adrianyotov.properties.MailConfigBean;
import com.adrianyotov.properties.PropertyBean;
import com.adrianyotov.properties.propertiesmanager.PropertiesManager;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Starts up the main application with GUI and does checks for
 * The MailConfig.properties file and opens the form for it should there be a problem
 * 
 * @author Adrian Yotov
 */
public class MainAppFX extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    private MailConfigBean bean;
    private PropertiesManager mng;
    private Stage primaryStage;
    private Parent rootLayout;
    private Locale currentLocale;

    
    /**
     * Starts the program with the main window but also uses initRootLayout()
     * To do a check for the config property file and will open the config form
     * So that the user can input their information before opening the Main window
     * 
     * @param primary
     * @throws Exception 
     */
    public void start(Stage primary) throws Exception {
        mng = new PropertiesManager();
        bean = new MailConfigBean();
        this.primaryStage = primary;

        initRootLayout();

        primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("Title"));
        this.primaryStage.getIcons().add(
                new Image(MainAppFX.class
                        .getResourceAsStream("/images/mail.png")));
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
        LOG.info("Program started");
    }

    /**
     * Will initialize the layout and checks if a MailConfig.property file
     * exists If the file doesn't exist, the program starts with the Config
     * form, otherwise The program simply takes the existing config file and
     * loads the data
     *
     * @throws IOException
     */
    public void initRootLayout() throws IOException {
        currentLocale = Locale.getDefault();
        LOG.debug("Locale = " + currentLocale);

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
        if (!(validMailConfig())) {
            
            LOG.info("Mail Config properties missing, showing form.");

            // Opening the config since something is either incorrect or the file is missing
            FXMLLoader secondLoader = new FXMLLoader();
            Stage settingsStage = new Stage();
            secondLoader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
            secondLoader.setLocation(MainAppFX.class
                    .getResource("/fxml/ConfigFXML.fxml"));

            Parent settingsRoot = secondLoader.load();
            ConfigFXMLController config = secondLoader.getController();
            config.setUpProperties(mng, bean);

            settingsStage.setScene(new Scene(settingsRoot));
            settingsStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("Title"));
            settingsStage.getIcons().add(
                new Image(MainAppFX.class
                        .getResourceAsStream("/images/cog.png")));
            settingsStage.initModality(Modality.APPLICATION_MODAL);
            settingsStage.showAndWait();
        }
        
        loader.setLocation(MainAppFX.class
                .getResource("/fxml/RootFXML.fxml"));
        rootLayout = (BorderPane) loader.load();

    }

    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }

    /**
     * Used to check if a MailConfig.property file is valid
     * Implementation for not allowing saving of empty fields not added yet
     * 
     * @return True if file is valid, false otherwise
     * @throws IOException
     */
    private boolean validMailConfig() throws IOException {

        return mng.loadTextProperties(bean, "", "MailConfig");
    }

}
