/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import com.adrianyotov.properties.EmailBean;
import com.adrianyotov.properties.FolderBean;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Adrian Yotov
 */
public interface EmailDAO {

    
    // Create
    public int createEmail(EmailBean mailData) throws SQLException;
    
    public int createFolder(String fName) throws SQLException;
    // Read
    public ArrayList<EmailBean> findAll() throws SQLException;
    
    public EmailBean findID(int id) throws SQLException;
    
    public ArrayList<EmailBean> findInFolder(int id) throws SQLException;
    
    public List<EmailBean> findSearchTerm(String search) throws SQLException;
    
    public List<EmailBean> findAuthor(String auth) throws SQLException;
    
    public ObservableList<FolderBean> findFolders() throws SQLException;
    
    public void saveEmailAttachments(int emailID) throws SQLException;
        
    public ArrayList<String> findEmailAttachmentNames(int emailID) throws SQLException;


    
    // Update
    public int updateDraftToSend(int id) throws SQLException;
    
    public int updateDraft(EmailBean email) throws SQLException;

    public int updateEmailToFolder(int emailID, String newName) throws SQLException;
    
    public int updateFolderName(String currentName,String newName) throws SQLException;
    
    
    // Delete
    public int deleteEmail(int ID) throws SQLException;
    
    public int deleteFolder(String fName) throws SQLException;

    public int getFolderID(String folderName) throws SQLException;
    
}
