package persistence;

import com.adrianyotov.properties.EmailBean;
import com.adrianyotov.properties.EmailTableBean;
import com.adrianyotov.properties.FolderBean;
import com.adrianyotov.properties.MailConfigBean;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;

/**
 * This class is used to store emails into a database as well as retrieve said
 * emails and update them when necessary
 *
 * @author Adrian Yotov
 */
public class EmailDAOImpl implements EmailDAO {

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOImpl.class);
    MailConfigBean userInfo;
    String URL;
    public EmailDAOImpl(MailConfigBean bean) {
        userInfo = bean;
        this.URL = "jdbc:mysql://"+bean.getUrlDB()+":"+ bean.getMysqlPort()+"/"+bean.getMysqlDatabase();
    }

    /**
     * Used to create email records into the database, inserting into multiple
     * tables as necessary
     *
     * @param mailData
     * @return the amount of records created in the database
     * @throws SQLException
     */
    @Override
    public int createEmail(EmailBean mailData) throws SQLException {
        int result;
        String query = "INSERT INTO Email (FolderID, FromField, Msg, HTMLMsg, Subj, SendDate, ReceivedDate) VALUES (?, ?, ?, ?, ?, ?, ?)";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            fillEmailPrepStmt(stmt, mailData);
            result = stmt.executeUpdate();
            setItemID(stmt, mailData);
        }
        checkToCreateAddresses(mailData);
        insertAttachmentsIntoMailBean(mailData);
        LOG.info("Records made: " + result);
        return result;
    }

    /**
     * Creates a new folder if it doesn't exist already
     *
     * @param fName
     * @return
     * @throws SQLException
     */
    @Override
    public int createFolder(String fName) throws SQLException {
        int result = -1;
        if (editableFolder(fName)) {

            if (!(folderExists(fName))) {

                String createQuery = "INSERT INTO Folders (fName) VALUES (?)";
                //Make the connection in try with resources so it auto closes at the end
                try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());//Use prepared statements to defend against SQL injection
                          PreparedStatement stmt = connection.prepareStatement(createQuery);) {
                    stmt.setString(1, fName);
                    result = stmt.executeUpdate();
                }
                LOG.info("Folders created: " + result);

            } else {

                throw new SQLException("Cannot create a folder that already exists");
            }

        } else {

            throw new SQLException("Cannot create a new Main Folder");
        }
        return result;

    }

    private boolean editableFolder(String fName) throws SQLException {
        if (fName.equals("Inbox") || fName.equals("Sent") || fName.equals("Draft")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean folderExists(String fName) throws SQLException {
        if (getFolderID(fName) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Used to see whether a given email address exists or not and will call
     * checkAddressID to find out. Will create a new address record as well as
     * insert its ID into the bridging table for emails and addresses
     *
     * @param mailData
     * @throws SQLException
     */
    private void checkToCreateAddresses(EmailBean mailData) throws SQLException {
        for (EmailAddress address : mailData.email.to()) {
            int addrID = -1;
            addrID = checkAddressID(address.toString());
            if (addrID == -1) {
                // creating new email address here if the ID is not found
                addrID = createAddress(address.toString());
            }
            insertIntoETABridging(mailData.getID(), addrID, "to");
        }

        for (EmailAddress address : mailData.email.cc()) {
            int addrID = -1;
            addrID = checkAddressID(address.toString());
            if (addrID == -1) {
                // creating new email address here if the ID is not found
                addrID = createAddress(address.toString());
            }
            insertIntoETABridging(mailData.getID(), addrID, "cc");

        }

        for (EmailAddress address : mailData.email.bcc()) {
            int addrID = -1;
            addrID = checkAddressID(address.toString());
            if (addrID == -1) {
                // creating new email address here if the ID is not found
                addrID = createAddress(address.toString());
            }
            insertIntoETABridging(mailData.getID(), addrID, "bcc");

        }

    }

    /**
     * Used to figure out an email address' ID unless it doesn't exist
     *
     * @param addr
     * @return the ID of the address, -1 if it doesn't exist
     * @throws SQLException
     */
    private int checkAddressID(String addr) throws SQLException {
        String query = "SELECT AddressID FROM Addresses WHERE Address = ?";
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setString(1, addr);
            try ( ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    //  returning the ID if it exists already
                    return rs.getInt(1);
                }
            }
        }
        // otherwise we return -1 since the ID was not found
        return -1;
    }

    private EmailBean createMailData(ResultSet rs) throws SQLException {
        EmailBean bean = new EmailBean();
        bean.setID(rs.getInt("EmailID"));
        bean.setFolderID(rs.getInt("FolderID"));
        bean.email.from(rs.getString("FromField"));

        bean.email.textMessage(rs.getString("Msg"));
        bean.email.htmlMessage(rs.getString("HTMLMsg"));

        bean.email.subject(rs.getString("Subj"));

        // checking if received/sent dates are null  and setting them if they are not null
        if (rs.getTimestamp("SendDate") == null) {
            bean.setSentDate(null);
        } else {
            bean.setSentDate(rs.getTimestamp("SendDate").toLocalDateTime());
        }
        if (rs.getTimestamp("SendDate") == null) {
            bean.email.sentDate(null);
        } else {
            bean.email.sentDate(new Date(rs.getTimestamp("SendDate").getTime()));
        }
        convertRegAttachToBean(bean);
        convertEmbAttachToBean(bean);
        convertReceiverToBean(bean, "to");
        convertReceiverToBean(bean, "cc");
        convertReceiverToBean(bean, "bcc");
        return bean;
    }

    private void convertReceiverToBean(EmailBean bean, String sentTo) throws SQLException {
        ArrayList<String> toEmails = new ArrayList<>();
        String query = "SELECT a.Address FROM Addresses a "
                + "JOIN EmailToAddress eta ON a.AddressID = eta.AddressID "
                + "JOIN Email e ON eta.EmailID = e.EmailID "
                + "WHERE e.EmailID = ? AND eta.ReceiveFields = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareCall(query);) {
            stmt.setInt(1, bean.getID());
            stmt.setString(2, sentTo);
            try ( ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    toEmails.add(rs.getString("a.Address"));
                }
            }
            if (sentTo.equals("to")) {
                toEmails.forEach(email -> {
                    bean.email.to(email);
                });
            }

            if (sentTo.equals("cc")) {
                toEmails.forEach(email -> {
                    bean.email.cc(email);
                });
            }
            if (sentTo.equals("bcc")) {
                toEmails.forEach(email -> {
                    bean.email.bcc(email);
                });
            }
        }
    }

    private void convertRegAttachToBean(EmailBean bean) throws SQLException {
        ArrayList<byte[]> atts = new ArrayList<>();
        ArrayList<String> files = new ArrayList<>();
        String query = "SELECT a.Filename, a.BinaryData FROM Attachments a "
                + "JOIN Email e ON a.EmailID = e.EmailID "
                + "WHERE a.ContentID IS NULL OR a.ContentID = '' AND e.EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, bean.getID());
            try ( ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    atts.add(rs.getBytes("BinaryData"));
                    files.add(rs.getString("Filename"));
                }
            }
            for (int i = 0; i < files.size(); i++) {
                bean.email.attachment(EmailAttachment.with().content(atts.get(i)).name(files.get(i)));
            }
        }
    }

    private void convertEmbAttachToBean(EmailBean bean) throws SQLException {
        ArrayList<byte[]> atts = new ArrayList<>();
        ArrayList<String> files = new ArrayList<>();
        ArrayList<String> cIDs = new ArrayList<>();
        String query = "SELECT a.BinaryData, a.Filename, a.ContentID FROM Attachments a "
                + "JOIN Email e ON a.EmailID = e.EmailID "
                + "WHERE a.ContentID IS NOT NULL AND a.ContentID != '' AND e.EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, bean.getID());
            try ( ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    atts.add(rs.getBytes("BinaryData"));
                    files.add(rs.getString("Filename"));
                    cIDs.add(rs.getString("ContentID"));
                }
            }
            for (int i = 0; i < files.size(); i++) {
                bean.email.embeddedAttachment(EmailAttachment.with().content(atts.get(i)).name(files.get(i)).contentId(cIDs.get(i)));
            }
        }
    }

    /**
     * Used to insert a new email address
     *
     * @param address
     * @return the ID of the newly created address
     * @throws SQLException
     */
    private int createAddress(String address) throws SQLException {
        int addrID = -1;
        String query = "INSERT INTO Addresses (Address) VALUES (?)";
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            stmt.setString(1, address);
            stmt.executeUpdate();
            try ( ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    addrID = rs.getInt(1);
                }
            }
        }
        LOG.info("New address created with ID: " + addrID);
        return addrID;
    }

    /**
     * Used to insert into the EmailToAddress bridging table to know which
     * address(es) in an email are considered as what type of receiver (to, cc
     * or bcc)
     *
     * @param mailID
     * @param addrID
     * @param sentTo
     * @throws SQLException
     */
    private void insertIntoETABridging(int mailID, int addrID, String sentTo) throws SQLException {
        String query = "";
        int result;
        if (sentTo.equals("to")) {
            query = "INSERT INTO EmailToAddress(EmailID, AddressID, ReceiveFields) VALUES (?, ?, ?)";
            try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
                stmt.setInt(1, mailID);
                stmt.setInt(2, addrID);
                stmt.setString(3, sentTo);
                result = stmt.executeUpdate();
                LOG.info("Record added to EmailToAddress: " + result);
            }
        }
        if (sentTo.equals("cc")) {
            query = "INSERT INTO EmailToAddress(EmailID, AddressID, ReceiveFields) VALUES (?, ?, ?)";
            try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
                stmt.setInt(1, mailID);
                stmt.setInt(2, addrID);
                stmt.setString(3, sentTo);
                result = stmt.executeUpdate();
                LOG.info("Record added to EmailToAddress: " + result);
            }
        }
        if (sentTo.equals("bcc")) {
            query = "INSERT INTO EmailToAddress(EmailID, AddressID, ReceiveFields) VALUES (?, ?, ?)";
            try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
                stmt.setInt(1, mailID);
                stmt.setInt(2, addrID);
                stmt.setString(3, sentTo);
                result = stmt.executeUpdate();
                LOG.info("Record added to EmailToAddress: " + result);
            }
        }
    }

    private void insertAttachmentsIntoMailBean(EmailBean mailData) throws SQLException {
        if (mailData.email.attachments().isEmpty()) {

        } else {
            for (EmailAttachment att : mailData.email.attachments()) {
                LOG.info("contentid: " + att.getContentId());
                if (att.getContentId() != null) {
                    insertEmbedAttach(att, mailData.getID());
                } else {
                    insertRegAttach(att, mailData.getID());
                }
            }
        }
    }

    private int insertRegAttach(EmailAttachment att, int emailID) throws SQLException {
        int result = -1;
        String query = "INSERT INTO Attachments (EmailID, BinaryData, Filename) VALUES (?, ?, ?)";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, emailID);
            stmt.setBytes(2, att.toByteArray());
            stmt.setString(3, att.getName());
            result = stmt.executeUpdate();
            LOG.info(result + "Attachments added");
        }

        return result;
    }

    private int insertEmbedAttach(EmailAttachment att, int emailID) throws SQLException {
        int result = -1;
        String query = "INSERT INTO Attachments (EmailID, ContentID, BinaryData, Filename) VALUES (?, ?, ?, ?)";
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, emailID);
            stmt.setString(2, att.getContentId());
            stmt.setBytes(3, att.toByteArray());
            stmt.setString(4, att.getName());
            result = stmt.executeUpdate();
            LOG.info(result + "Embedded attachments added");
        }

        return result;
    }

    /**
     * Used to set an an object's ID
     *
     * @param stmt
     * @return The ID of the object
     * @throws SQLException
     */
    private void setItemID(PreparedStatement stmt, EmailBean bean) throws SQLException {
        int result = -1;

        try ( ResultSet rs = stmt.getGeneratedKeys();) {
            if (rs.next()) {
                result = rs.getInt(1);
            }

        }
        LOG.info("New ID is: " + result);
        bean.setID(result);
    }

    /**
     * Used to find every email in the database
     *
     * @return The list of emails
     * @throws SQLException
     */
    @Override
    public ArrayList<EmailBean> findAll() throws SQLException {
        ArrayList<EmailBean> mailData = new ArrayList<>();

        String query = "SELECT EmailID, FolderID, FromField, Msg, HTMLMsg, Subj, ReceivedDate, SendDate FROM Email";
        LOG.info(query);
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());
                PreparedStatement stmt = connection.prepareStatement(query); 
                ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                mailData.add(createMailData(rs));
            }
            LOG.info("Records found: " + mailData.size());
        }
        return mailData;
    }
    
    @Override
    public void saveEmailAttachments(int emailID) throws SQLException {
        String query = "SELECT Filename, BinaryData FROM Attachments WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());
                PreparedStatement stmt = connection.prepareStatement(query);) {
            
            stmt.setInt(1, emailID);
            ResultSet rs = stmt.executeQuery();


            while (rs.next()) {
                String filename = rs.getString("Filename");
                File item = new File(filename);
                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(item);
                    byte[] buffer = new byte[1];
                    InputStream in = rs.getBinaryStream("BinaryData");
                    while (in.read(buffer) > 0) {
                        fos.write(buffer);
                    }

                    fos.close();
                } catch (FileNotFoundException e) {
                    LOG.info(e.getMessage());
                }

            }
        } catch (IOException ex) {
            LOG.info(ex.getMessage());
        }
    }
    
    
    @Override
    public ArrayList<String> findEmailAttachmentNames(int emailID) throws SQLException {
        ArrayList<String> attachments = new ArrayList<>();
        String query = "SELECT Filename FROM Attachments WHERE EmailID = ? AND ContentID IS NOT NULL";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());
                PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, emailID);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                attachments.add(rs.getString(1));
            }
        }
        return attachments;
    }

    /**
     * Used to find an email by its ID
     *
     * @param id
     * @return The specific email
     * @throws SQLException
     */
    @Override
    public EmailBean findID(int id) throws SQLException {
        EmailBean mailData = new EmailBean();
        String query = "SELECT EmailID, FolderID, FromField, Msg, HTMLMsg, Subj, ReceivedDate, SendDate FROM Email "
                + "WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, id);
            try ( ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    mailData = createMailData(rs);
                }
            }
        }
        LOG.info("Record's subject by ID: " + mailData.email.subject());
        return mailData;
    }

    @Override
    public ArrayList<EmailBean> findInFolder(int id) throws SQLException {
        ArrayList<EmailBean> mailData = new ArrayList<>();

        String query = "SELECT EmailID, FolderID, FromField, Msg, HTMLMsg, Subj, ReceivedDate, SendDate FROM Email WHERE FolderID = ?";
        LOG.info(query);
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());
                PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mailData.add(createMailData(rs));
            }

            LOG.info("Records found: " + mailData.size());
        }
        return mailData;
    }

    /**
     * Used to search for an email with a search term for the email's subject
     *
     * @param toSearch
     * @return The list of emails that may contain the search term
     * @throws SQLException
     */
    @Override
    public List<EmailBean> findSearchTerm(String toSearch) throws SQLException {
        List<EmailBean> mailData = new ArrayList<>();
        String query = "SELECT EmailID, FolderID, FromField, Msg, HTMLMsg, Subj, SendDate, ReceivedDate FROM Email "
                + "WHERE Subj LIKE ?";
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setString(1, "%" + toSearch + "%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mailData.add(createMailData(rs));
            }
        }
        LOG.info("Records by search terms: " + mailData.size());
        return mailData;
    }

    /**
     * Used to find an email by its author
     *
     * @param auth
     * @return The list of emails possibly sent by the given author
     * @throws SQLException
     */
    @Override
    public List<EmailBean> findAuthor(String auth) throws SQLException {
        List<EmailBean> mailData = new ArrayList<>();
        String query = "SELECT e.EmailID, e.FolderID, e.FromField, e.Msg, e.HTMLMsg, e.Subj, e.SendDate, e.ReceivedDate, a.BinaryData, a.Filename FROM Email e "
                + "JOIN Attachments a ON a.EmailID = e.EmailID "
                + "WHERE FromField = ?";
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, auth);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mailData.add(createMailData(rs));
            }
        }
        LOG.info("Records by author: " + mailData.size());
        return mailData;
    }

    /**
     * Used to find all folders
     *
     * @return a list of existing folders
     * @throws SQLException
     */
    @Override
    public ObservableList<FolderBean> findFolders() throws SQLException {
        ObservableList<FolderBean> folders = FXCollections.observableArrayList();
        String query = "SELECT FolderID, fName FROM Folders";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                folders.add(createFolderData(rs));
            }
        }
        return folders;
    }

    private FolderBean createFolderData(ResultSet rs) throws SQLException {
        FolderBean folder = new FolderBean();
        folder.setId(rs.getInt("FolderID"));
        folder.setfName(rs.getString("fName"));
        return folder;
    }

    /**
     * Used to update a draft email to become a sent one
     *
     * @param mail
     * @return If a record was updated or not
     * @throws SQLException
     */
    @Override
    public int updateDraftToSend(int id) throws SQLException {
        int result;
        String query = "UPDATE Email SET FolderID=?, SendDate=? WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            EmailBean mail = findID(id);
            stmt.setInt(1, getFolderID("Sent"));
            stmt.setTimestamp(2, new Timestamp(new Date().getTime()));
            stmt.setInt(3, id);
            result = stmt.executeUpdate();
        }
        LOG.info("Records updated: " + result);
        return result;
    }

    @Override
    public int getFolderID(String folder) throws SQLException {
        int id = -1;
        String query = "SELECT FolderID FROM Folders WHERE fName = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, folder);

            try ( ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    id = rs.getInt("FolderID");
                }
            }
        }
        return id;
    }

    /**
     * Used to delete and email based on its ID
     *
     * @param ID
     * @return If the email was deleted or not
     * @throws SQLException
     */
    @Override
    public int deleteEmail(int ID) throws SQLException {
        int result;
        String query = "DELETE FROM Email WHERE EmailID = ?";
        // Removing entries related to the email being deleted
        flushSpecificReceivers(ID);
        flushSpecificAttach(ID);
        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, ID);
            result = stmt.executeUpdate();
        }
        LOG.info("Records deleted: " + result);
        return result;
    }

    /**
     * Used to set a PreparedStatement's values with an EmailBean's data
     *
     * @param stmt
     * @param bean
     * @throws SQLException
     */
    private void fillEmailPrepStmt(PreparedStatement stmt, EmailBean bean) throws SQLException {
        stmt.setInt(1, bean.getFolderID());
        stmt.setString(2, bean.email.from().toString());
        stmt.setString(3, bean.email.messages().get(0).getContent());
        stmt.setString(4, bean.email.messages().get(1).getContent());
        stmt.setString(5, bean.email.subject());
        if (bean.email.sentDate() == null) {
            stmt.setTimestamp(6, null);
        } else {
            stmt.setTimestamp(6, new Timestamp(bean.email.sentDate().getTime()));
        }
        if (bean.getRecDate() == null) {
            stmt.setTimestamp(7, null);
        } else {
            stmt.setTimestamp(7, Timestamp.valueOf(bean.getRecDate()));
        }
    }

    /**
     * Used to update a draft's contents
     *
     * @param bean
     * @return
     * @throws SQLException
     */
    @Override
    public int updateDraft(EmailBean bean) throws SQLException {
        int result;
        String query = "UPDATE Email SET  Msg = ?, HTMLMsg = ?, Subj = ? WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setString(1, bean.email.messages().get(0).getContent());
            stmt.setString(2, bean.email.messages().get(1).getContent());
            stmt.setString(3, bean.email.subject());
            stmt.setInt(4, bean.getID());

            result = stmt.executeUpdate();
        }
        LOG.info("Draft updated: " + result);
       flushSpecificReceivers(bean.getID());
        flushSpecificAttach(bean.getID());
        insertAttachmentsIntoMailBean(bean);
        checkToCreateAddresses(bean);
        return result;
    }

    private void flushSpecificReceivers(int emailID) throws SQLException {
        int result;
        String query = "DELETE FROM EmailToAddress WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, emailID);
            result = stmt.executeUpdate();
        }
        LOG.info("Records flushed from EmailToAddress: " + result);
    }

    private void flushSpecificAttach(int emailID) throws SQLException {
        int result;
        String query = "DELETE FROM Attachments WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, emailID);
            result = stmt.executeUpdate();
        }
        LOG.info("Attachments flushed: " + result);
    }

    /**
     * Send an email to a different folder
     *
     * @param emailID
     * @param folderName
     * @return
     * @throws SQLException
     */
    @Override
    public int updateEmailToFolder(int emailID, String folderName) throws SQLException {
        int result;
        EmailBean bean = this.findID(emailID);
        if (folderName.equals("Draft")) {
            throw new SQLException("Cannot send emails to draft");
        }
        String query = "UPDATE Email SET FolderID = ? WHERE EmailID = ?";

        try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
            stmt.setInt(1, getFolderID(folderName));
            stmt.setInt(2, bean.getID());
            result = stmt.executeUpdate();
            bean.setFolderID(getFolderID(folderName));
        }
        LOG.info("Record move to different folder: " + result);
        return result;
    }

    /**
     * Used to change a folder's name
     *
     * @param currentName
     * @param newName
     * @return
     * @throws SQLException
     */
    @Override
    public int updateFolderName(String currentName, String newName) throws SQLException {
        if (editableFolder(currentName)) {
            int result = 0;
            int id = getFolderID(currentName);
            String query = "UPDATE Folders SET fName = ? WHERE FolderID = ?";

            try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
                stmt.setString(1, newName);
                stmt.setInt(2, id);
                result = stmt.executeUpdate();
            }
            LOG.info("Folder updated : " + result);
            return result;
        }
        return 0;
    }

    /**
     * Used to delete a folder if it is allowed
     *
     * @param fName
     * @return
     * @throws SQLException
     */
    @Override
    public int deleteFolder(String fName) throws SQLException {
        if (editableFolder(fName)) {
            int result;
            String query = "DELETE FROM Folders  WHERE FolderID = ?";

            try ( Connection connection = DriverManager.getConnection(URL, userInfo.getDbUserName(), userInfo.getDbPassword());  PreparedStatement stmt = connection.prepareStatement(query);) {
                stmt.setInt(1, getFolderID(fName));
                result = stmt.executeUpdate();
            }
            LOG.info("Folder deleted: " + result);
            return result;
        } else {
            throw new SQLException("Cannot delete this folder");
        }
    }

}
