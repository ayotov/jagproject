
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF  EXISTS EmailToAddress;

CREATE TABLE EmailToAddress(
EmailID INT,
AddressID INT,
ReceiveFields VARCHAR(3), 
FOREIGN KEY fkemailid (EmailID) REFERENCES Email(EmailID),
FOREIGN KEY fkaddressid (AddressID) REFERENCES Addresses(AddressID)
);
SET FOREIGN_KEY_CHECKS=1;

INSERT INTO EmailToAddress (EmailID, AddressID, ReceiveFields) VALUES
(1, 2, 'to'),
(1, 3, 'cc'),
(2, 4, 'to'),
(2, 3, 'bcc'),
(3, 5, 'to'),
(4, 1, 'to'),
(5, 3, 'to');