SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS Email;


CREATE TABLE Email(
EmailID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
FolderID INT NOT NULL,
FromField VARCHAR(320) DEFAULT '',
Msg TEXT,
HTMLMsg TEXT,
Subj VARCHAR(100) DEFAULT '',
SendDate TIMESTAMP DEFAULT null,
ReceivedDate TIMESTAMP DEFAULT null,
FOREIGN KEY fkfolderid (FolderID) REFERENCES Folders(FolderID)
);
SET FOREIGN_KEY_CHECKS=1;


INSERT INTO Email (FolderID, FromField, Msg, HTMLMsg, Subj, SendDate, ReceivedDate) VALUES
(1, 'jimbo@com.com', 'boom', '<p>kaboom</p>', 'subject', null, CURRENT_TIMESTAMP),
(2, 'bonnie@fox.com', 'message', '<p>html</p>', 'subj', CURRENT_TIMESTAMP, null),
(1, 'mary@read.com', 'smsg', '<p>htmlmsg</p>', 'sbj', null, CURRENT_TIMESTAMP),
(3, 'atalante@hunt.com', 'sms', '<p>hmtlsmg</p>', 'sub', null, null),
(2, 'skadi@giant.com', 'mss', '<p>mthlmsg</p>', 'sjb', CURRENT_TIMESTAMP, null);